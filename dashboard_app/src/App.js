import React from 'react';
import './App.css';
import './components/MachineList';
import './components/MachineInfo';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import MachineList from './components/MachineList';
import MachineInfo from './components/MachineInfo';
import Navbar from './components/Navbar';


function App() {
  return (
    <Router>
    <div className="container">
      <Navbar />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/m" exact component={MachineList} />
          <Route path="/i/:machineId" component={MachineInfo} />
        </Switch>
      </div>  
    </Router>
  );
}

const Home = () => (
  <div>
    <h1>MachineTracker home page</h1>
    <div className="md-form mt-0">
    </div>
  </div>
)

export default App;
