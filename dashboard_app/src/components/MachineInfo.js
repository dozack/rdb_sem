import React, {useState, useEffect} from 'react';
import { Tab, Tabs} from 'react-bootstrap';
import SensorPlot from './SensorPlot';

function MachineInfo({ match }) {

  useEffect(() => {
    fetchInfo();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  const [node, setNode] = useState({});
  const [configs, setConfigs] = useState([]);
  const [sensors, setSensors] = useState([]);

  const fetchInfo = async () => {
    const data = await fetch("http://localhost:5000/api/i/" + match.params.machineId)
    const items = await data.json();
    setNode(items.node);
    setConfigs(items.configs);
    setSensors(items.sensors);
  }

  return (
    <div>
      <div className="jumbotron">
        <h3>Informace o zařízení</h3>
        <p className="lead">SN: {node.nodeId}</p>
      </div>
      <div>
      <div style={{marginBottom:25, paddingTop: 15}}><h4>Nalezené konfigurace</h4></div>
        <Tabs id={node._id}>
            {configs.map(config =>
              <Tab key={config.configId} eventKey={config._id} title={config.nodeName}>
                <br/>
                <table className="table table-hover">
                  <tbody>
                    <tr>
                      <td>ID</td>
                      <td>{config.configId}</td>
                    </tr>
                    <tr>
                      <td>Platná od</td>
                      <td>{new Date(config.createDate).toUTCString('en-US')}</td>
                    </tr>
                  </tbody>
                </table>
                <div className="">
                  <div style={{marginBottom:50, paddingTop: 15}}><h4>Data ze senzorů za posledních 24 hodin</h4></div>
                  {sensors.filter(function(a) { return a.configId === config.configId}).map(sensor =>
                    <div key={sensor._id} className="card bg-light" style={{marginBottom:15}}> 
                      <div className="card-header">
                        {sensor.sensorId} - {sensor.sensorType}
                      </div>
                      <div className="card-body">
                      <SensorPlot key={sensor._id} sensorId={sensor.sensorId} sensorType={sensor.sensorType}/>
                      </div> 
                    </div>
                    
                  )}
                   
                </div>
                    
              </Tab>
            )}
        </Tabs>
      </div>
    </div>  
  );
}

export default MachineInfo;