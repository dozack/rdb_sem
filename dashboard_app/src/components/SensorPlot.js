// eslint-disable-next-line getter-return
import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import CanvasJSReact from '../canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class SensorPlot extends Component {	

  constructor(props) {
    super(props);
    this.state = {
      chartData : [],
      loaded : false
    }
  }
   
  componentDidMount() {
    this.fetchData(res => {
      console.log(res);
    });
  }

  shouldComponentUpdate() {
        return true;
  }

  // eslint-disable-next-line no-undef
  fetchData = async () => {
    const now_ = new Date();
    const dayAgo_ = new Date();
    const isGPS = (this.props.sensorType === 'GPS') ? true : false;
    dayAgo_.setHours(dayAgo_.getHours() - 24);

    var fetchedData = await fetch("http://localhost:5000/api/d/"+ this.props.sensorId + "/" + dayAgo_ + "/" + now_)
    var jsonData = await fetchedData.json();
    
    if(!isGPS) {
      jsonData.data.map(data => {
        data.type = "spline";
        return data.dataPoints.map(point => {
          return point.x = new Date(point.x);
        })
      })
      
      this.state.data = jsonData.data;
      this.state.lodaded = true;
    }
    this.setState({
      chartData: jsonData.data,
      loaded: true
    })
    return jsonData.data;
  }

  getUnits(type) {
    switch(type)
    {
      case "Acceleration":
        return "m/s^2";
      case "Temperature":
        return "°C";
      case "Voltage":
        return "V";
      case "Tension":
        return "N";
      case "GPS":
      default:
        return;
    }
  }

  render() {
   const options = {
     backgroundColor: "#f8f9fa",
      title: {
      fontWeight: "lighter",
      fontColor: "black",
      //fontSize: 20,
      //text: this.props.sensorId + " - " + this.props.sensorType
      },
      axisX:{
        gridThickness: 0,
      },
      axisY:{
        gridThickness: 0.1,
        lineThickness: 0,
        title: this.getUnits(this.props.sensorType)
      },
      data: this.state.chartData
   }
   const coords= this.state.chartData;
   if(this.props.sensorType === 'GPS')
   {
    console.log(coords);
    return (
      <div style={{width: '100%', height:'450px'}}>
        {
          // Replace with Leaflet
        }
        <GoogleMapReact
          bootstrapURLKeys={{key:'AIzaSyBWVI76vdgn5JgIiRR0Ba8rHIBtEKwQZ9M'}}
          defaultCenter={{lat: 50.74, lng: 15.04}}
          defaultZoom={15}
          options={{ gestureHandling: "greedy" }}
        >
        </GoogleMapReact>
      </div>
    );
   }
   else
   {
    return (
      <div style={{width: '100%', height:'450px'}}>
          <CanvasJSChart options = {options}/>
          <br/>
      </div>
    );
   }
  }
}

  export default SensorPlot;      