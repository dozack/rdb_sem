import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';

function MachineList() {

  useEffect(() => {
    fetchMachines();
  },[]);

  const [items, setItems] = useState([]);

  const fetchMachines = async () => {
    const data = await fetch("http://localhost:5000/api/m");
  
    const items = await data.json();
    setItems(items);
  }

  return (
    <div>
      <div className="jumbotron">
        <h3>Přehled</h3>
        <p className="lead">Stroje nalezené v databázi</p>
      </div>
      <table className="table table-hover">
      <thead>
        <tr>
          <th scope="col">VIN kód zařízení</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        {items.map(item =>(
            <tr key={item._id}>
              <td>{item.machineId}</td>
              <td><Link className="float-right" to={`i/${item.machineId}`}>Detaily</Link></td>
            </tr>
          ))}
      </tbody>
    </table>
    </div>
  );
}

export default MachineList;