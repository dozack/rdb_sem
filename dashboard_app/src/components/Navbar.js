import React from 'react';
import { Link } from 'react-router-dom';

function Navbar() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
        <ul className="navbar-nav"><Link className="nav-link" to="/">MachineTracker</Link></ul> 
        <ul className="navbar-nav"><Link className="nav-link" to="/m">Přehled</Link></ul>
    </nav>
  );
}

export default Navbar;