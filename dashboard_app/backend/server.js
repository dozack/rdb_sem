const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');
const app = express();

require('dotenv/config');
const port = process.env.PORT || 5000;


app.use(cors());
app.use(express.json());

const uri = process.env.MONGO_URI;
mongoose
.connect(uri, {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true})
.catch(err => console.log(err));

const connection = mongoose.connection;
connection.once('open', () => {
});

const api = require('./routes/api');

app.use('/api',api);

app.listen(port, () => {
    console.log('************************************************************************************');
    console.log('[SERVER] listening at', port);
    console.log('************************************************************************************');
});