const router = require('express').Router();
const SensorData = require('../models/sensordata.model');

// Do not use
/*
router.route('/').get( async (req, res) => {
    await SensorData.find()
    .then(data => res.json(data))
    .catch(err => res.status(400).json('Error: ',err));
});
*/

// GENERAL
// -------------------------------------------------------------------------------------------
router.route('/:dataId').get( async (req,res) => {
    await SensorData.find({ dataId: req.params.dataId})
    .then(data => res.json(data))
    .catch(err => res.status(400).json('Error: ',err));
});

// SENSOR BASED LOOKUP
// -------------------------------------------------------------------------------------------
router.route('/sensor-id/:sensorId').get( async (req, res) => {
    await SensorData.find({ sensorId: req.params.sensorId})
    .then(data => res.json(data))
    .catch(err => res.status(400).json('Error: ',err));
});

router.route('/:sensorId/:from/:to').get( async (req, res) => {
    const from_ = new Date(req.params.from);
    const to_ = new Date(req.params.to);
    await SensorData.find({sensorId: req.params.sensorId, publishedAt:{$gt:from_, $lt:to_ }})
    .then(data => res.json(data))
    .catch(err => res.status(400).json('Error: ',err));
});

router.route('/sensor-id/:sensorId/last').get( async (req, res) => {
    await SensorData.findOne({ sensorId: req.params.sensorId})
    .then(data => res.json(data))
    .catch(err => res.status(400).json('Error: ', err));
});

// NODE BASED LOOKUP
// -------------------------------------------------------------------------------------------
router.route('/node-id/:nodeId').get( async (req, res) => {
    await SensorData.find({ nodeId: req.params.nodeId})
    .then(data => res.json(data))
    .catch(err => res.status(400).json('Error: ',err));
});

router.route('/:nodeId/:from/:to').get( async (req, res) => {
    const from_ = new Date(req.params.from);
    const to_ = new Date(req.params.to);
    await SensorData.find({nodeId: req.params.nodeId, publishedAt:{$gt:from_, $lt:to_ }})
    .then(data => res.json(data))
    .catch(err => res.status(400).json('Error: ',err));
});

router.route('/sensor-id/:nodeId/last').get( async (req, res) => {
    await SensorData.findOne({ nodeId: req.params.nodeId})
    .then(data => res.json(data))
    .catch(err => res.status(400).json('Error: ', err));
});

module.exports = router;