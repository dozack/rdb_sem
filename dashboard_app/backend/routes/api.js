const router = require('express').Router();
const Machine = require('../models/machine.model');
const Node = require('../models/node.model');
const NodeConfig = require('../models/nodecfg.model');
const Sensor = require('../models/sensor.model');
const SensorData = require('../models/sensordata.model');

// get list of all machines
// -------------------------------------------------------------------------------------------
router.route('/m').get((req, res) => {
    Machine.find()
    .then(machines => res.json(machines))
    .catch(err => res.status(400).json(err));
});

// get information about machine 
// -------------------------------------------------------------------------------------------
router.route('/i/:machineId').get( (req,res) => {
    var node, config, sensors;
    Node.findOne({machineId:req.params.machineId})
    .then(data => {
        node = data;
        return data;
    })
    .then(function() {
        return NodeConfig.find({nodeId:node.nodeId})
        .then(data => {
            config = config || data;
            return data;
        })
        .catch(err => res.status(400).json(err));
    })
    .then(function() {
        return Promise.all(config.map(v => {
            return Sensor.find({configId: v.configId})
            .then(data => {
                return data;
            })
            .catch(err => res.status(400).json(err));
        }))
        .then(results => {
            return sensors = results.flat(1);
        })
        .catch(err => res.status(400).json(err));
    })
    .then(function() {

        sensors = [...new Set(sensors)];
        config = [...new Set(config)];

        var ans = {node: node, configs: config, sensors: sensors};
        res.json(ans);
    })
    .catch(err => res.status(400).json(err));
})

// get data from sensor in time interval
// -------------------------------------------------------------------------------------------
router.route('/d/:sensorId/:from/:to').get((req, res) => {
    const from_ = new Date(req.params.from);
    const to_ = new Date(req.params.to);
    var isGPS = false;
    Sensor.findOne({sensorId: req.params.sensorId})
    .then(sensor=>{
        isGPS = (sensor.sensorType === 'GPS') ? true : false;
        const data = SensorData.find({sensorId: sensor.sensorId, publishedAt:{$gte:from_, $lt:to_}})
        .catch(err => console.log(err));
        return data;
    })
    .then(data => {
        var response = {};
        response.sensorId = req.params.sensorId;
        response.data = [];
        if(!isGPS){
            for(var i = 0; i < data[0].publishedData.length; i++)
            {
                response.data[i] = {
                    type: "line",
                    dataPoints: []
                }
            }
            data.map(record => {
                record.publishedData.map((val,index ) => {
                    var point = {
                        x: new Date(record.publishedAt),
                        y: val
                    }
                    response.data[index].dataPoints.push(point);
                    return true;
                });
            });
        }
        else{
            data.map(record => {
                var point = {
                    latitude: record.publishedData[0],
                    longtitude: record.publishedData[1]
                }
                response.data.push(point);
                return true;
            })
        }
        return response;
    })
    .then(dataSet => {
        res.json(dataSet);
    })
    .catch(err => {
        console.log(err);
        res.status(400).json(err);
    });
});


module.exports = router;