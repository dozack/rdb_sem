const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const nodecfgSchema = new Schema({
    configId: {
        type:String,
        required: true,
        unique: true
    },
    nodeId: {
        type: String,
        required: true
    },
    createDate: {
        type: Date,
        required: true    
    },
    nodeName: {
        type: String,
        required: true  
    },
    publishTime: {
        type: Number,
        required: true   
    }
}, {collection: 'nodeConfig'});

const NodeConfig = mongoose.model('nodeConfig', nodecfgSchema);

module.exports = NodeConfig;