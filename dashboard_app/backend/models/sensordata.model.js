const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const sensordataSchema = new Schema({
    dataId: {
        type:String,
        required: true,
        unique: true
    },
    sensorId: {
        type: String,
        required: true 
    },
    nodeId: {
        type: String,
        required: true
    },
    publishedAt: {
        type: Date,
        required: true
    },
    publishedData: {
        type: Array,
        required: true
    }
}, {collection: 'sensorData'});

const SensorData = mongoose.model('sensorData', sensordataSchema);

module.exports = SensorData;