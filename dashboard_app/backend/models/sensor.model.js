const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const sensorSchema = new Schema({
    sensorId: {
        type:String,
        required: true,
        unique: true
    },
    configId: {
        type: String,
        required: true,
        unique: true       
    },
    sensorType: {
        type: String,
        required: true
    }
}, {collection: 'sensor'});

const Sensor = mongoose.model('sensor', sensorSchema);

module.exports = Sensor;