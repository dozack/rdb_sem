const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const nodeSchema = new Schema({
    nodeId: {
        type:String,
        required: true,
        unique: true
    },
    machineId: {
        type: String,
        required: true,
        unique: true       
    }
}, {collection: 'node'});

const Node = mongoose.model('node', nodeSchema);

module.exports = Node;