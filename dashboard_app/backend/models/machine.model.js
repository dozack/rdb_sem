const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const machineSchema = new Schema({
    machineId: {
        type: String,
        required: true,
        unique: true       
    }
}, {collection: 'machine'});

const Machine = mongoose.model('machine', machineSchema);

module.exports = Machine;