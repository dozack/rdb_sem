# Dashboard aplikace po vizualizaci obsahu databáze

## ToDo

- Vyhledávání v datech od-do (v REST API implementováno)
- Zprovoznění map(Leaflet?) a vykreslení Polyline dle dat (v REST API implementováno)

## Zapnutí generátoru dat

Generátor se připojuje na lokální instanci MongoDB, která by měla být spuštěna na portu 27017, vytvoří databázi machineStorage a v ní 5 kolekcí. 

1. Command line: `cd <repository_path>/data_generator/_RELEASE/`
2. `MongoManager.exe <NODE_COUNT> <DROP_DB>`

- `NODE_COUNT` - počet simulovaných jednotek
- `DROP_DB` - `-y` smaže předchozí data v MongoDB (pokud není vyžadováno, tak -y nepsat)

Příklad: `cd Desktop/_git/rdb_sem/_RELEASE/MongoManager.exe 10 -y` (smaže stará data a vytvoří 10 jednotek).  
 
## Oživení dashboardu

Potřebné nástroje:  
- [Visual Studio Code](https://code.visualstudio.com/download)  
- [Node.JS](https://nodejs.org/en/download/)  

1. Ve VS Code nainstalovat plugin `npm`
2. `File` -> `Open Folder...` -> `dashboard_app` v tomto repozitáři
3. Otevřít terminál (`Terminal` -> `New terminal`) a napsat: `npm install`
4. `cd backend` a opět `npm install` (jedná se o instalaci knihoven potřebných pro spuštění aplikace)
5. Ve složce backend napsat `nodemon server.js` (nodemon by měl být již nainstalován, pokud vrací chybu, tak `npm install nodemon`)
6. Teď už by měl běžet REST API server na portu 5000, s přístupem k databázi, nyní je třeba spustit frontend: otevřít nový terminál, jít do složky dashboard_app a `npm start`
7. Měla by se otevřít webová stránka s dashboardem

Jakýkoliv problém mi napište prosím do issues

## REST API příkazy

Pokud si chcete vyzkoušet samotné REST API, tak umožňuje následující funkce:

- `GET <REST_URL>/m` - vrátí seznam VIN kódů strojů v databázi
- `GET <REST_URL>/i/<MACHINE_VIN>` - najde jednotku patřící k VIN kódu `<MACHINE_VIN>`, její konfiguraci a seznam senzorů
- `GET <REST_URL>/d/<SENSOR_ID>/<FROM>/<TO>` - vypíše data ze senzoru s ID = `<SENSOR_ID>` v časovém intervalu `<FROM>`:`<TO>` (datum je ve formátu ISO 8601)

`<REST_URL>` je v základu http://localhost:5000 po spuštění backend serveru (viz **Oživení dashboardu**, krok 5)