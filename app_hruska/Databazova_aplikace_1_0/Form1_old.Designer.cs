﻿namespace Databazova_aplikace_1_0
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btn_StartStop = new System.Windows.Forms.Button();
            this.lbl_Status = new System.Windows.Forms.Label();
            this.btn_Smazat = new System.Windows.Forms.Button();
            this.btn_NactiDB = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btn_FiltrujData = new System.Windows.Forms.Button();
            this.textBox_Filter = new System.Windows.Forms.TextBox();
            this.comboBox_NamesFilter = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btn_StartStop
            // 
            this.btn_StartStop.BackColor = System.Drawing.Color.Lime;
            this.btn_StartStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_StartStop.Location = new System.Drawing.Point(24, 12);
            this.btn_StartStop.Name = "btn_StartStop";
            this.btn_StartStop.Size = new System.Drawing.Size(111, 34);
            this.btn_StartStop.TabIndex = 0;
            this.btn_StartStop.Text = "Start zápisu";
            this.btn_StartStop.UseVisualStyleBackColor = false;
            this.btn_StartStop.Click += new System.EventHandler(this.btn_StartStop_Click);
            // 
            // lbl_Status
            // 
            this.lbl_Status.AutoSize = true;
            this.lbl_Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_Status.ForeColor = System.Drawing.Color.Black;
            this.lbl_Status.Location = new System.Drawing.Point(141, 19);
            this.lbl_Status.Name = "lbl_Status";
            this.lbl_Status.Size = new System.Drawing.Size(216, 20);
            this.lbl_Status.TabIndex = 1;
            this.lbl_Status.Text = "Počet zapsaných záznamů: 0";
            // 
            // btn_Smazat
            // 
            this.btn_Smazat.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Smazat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Smazat.ForeColor = System.Drawing.Color.Red;
            this.btn_Smazat.Location = new System.Drawing.Point(570, 12);
            this.btn_Smazat.Name = "btn_Smazat";
            this.btn_Smazat.Size = new System.Drawing.Size(163, 34);
            this.btn_Smazat.TabIndex = 2;
            this.btn_Smazat.Text = "Smazat vše z databáze!!";
            this.btn_Smazat.UseVisualStyleBackColor = false;
            this.btn_Smazat.Click += new System.EventHandler(this.btn_Smazat_Click);
            // 
            // btn_NactiDB
            // 
            this.btn_NactiDB.BackColor = System.Drawing.SystemColors.Control;
            this.btn_NactiDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_NactiDB.Location = new System.Drawing.Point(413, 12);
            this.btn_NactiDB.Name = "btn_NactiDB";
            this.btn_NactiDB.Size = new System.Drawing.Size(111, 34);
            this.btn_NactiDB.TabIndex = 4;
            this.btn_NactiDB.Text = "Načti DB";
            this.btn_NactiDB.UseVisualStyleBackColor = false;
            this.btn_NactiDB.Click += new System.EventHandler(this.btn_NactiDB_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.Location = new System.Drawing.Point(12, 53);
            this.listBox1.Name = "listBox1";
            this.listBox1.ScrollAlwaysVisible = true;
            this.listBox1.Size = new System.Drawing.Size(1463, 654);
            this.listBox1.TabIndex = 5;
            // 
            // btn_FiltrujData
            // 
            this.btn_FiltrujData.BackColor = System.Drawing.SystemColors.Control;
            this.btn_FiltrujData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_FiltrujData.Location = new System.Drawing.Point(1364, 11);
            this.btn_FiltrujData.Name = "btn_FiltrujData";
            this.btn_FiltrujData.Size = new System.Drawing.Size(111, 34);
            this.btn_FiltrujData.TabIndex = 6;
            this.btn_FiltrujData.Text = "Filtrovat";
            this.btn_FiltrujData.UseVisualStyleBackColor = false;
            this.btn_FiltrujData.Click += new System.EventHandler(this.btn_FiltrujData_Click);
            // 
            // textBox_Filter
            // 
            this.textBox_Filter.Location = new System.Drawing.Point(997, 17);
            this.textBox_Filter.Multiline = true;
            this.textBox_Filter.Name = "textBox_Filter";
            this.textBox_Filter.Size = new System.Drawing.Size(361, 20);
            this.textBox_Filter.TabIndex = 7;
            this.textBox_Filter.Text = "<- nejdřív vyber hledaný tag";
            // 
            // comboBox_NamesFilter
            // 
            this.comboBox_NamesFilter.AccessibleName = "";
            this.comboBox_NamesFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_NamesFilter.FormattingEnabled = true;
            this.comboBox_NamesFilter.Location = new System.Drawing.Point(857, 16);
            this.comboBox_NamesFilter.Name = "comboBox_NamesFilter";
            this.comboBox_NamesFilter.Size = new System.Drawing.Size(134, 21);
            this.comboBox_NamesFilter.Sorted = true;
            this.comboBox_NamesFilter.TabIndex = 9;
            this.comboBox_NamesFilter.Tag = "";
            this.comboBox_NamesFilter.SelectedValueChanged += new System.EventHandler(this.comboBox_NamesFilter_SelectedValueChanged);
            this.comboBox_NamesFilter.Click += new System.EventHandler(this.comboBox_NamesFilter_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1487, 717);
            this.Controls.Add(this.comboBox_NamesFilter);
            this.Controls.Add(this.textBox_Filter);
            this.Controls.Add(this.btn_FiltrujData);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btn_NactiDB);
            this.Controls.Add(this.btn_Smazat);
            this.Controls.Add(this.lbl_Status);
            this.Controls.Add(this.btn_StartStop);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btn_StartStop;
        private System.Windows.Forms.Label lbl_Status;
        private System.Windows.Forms.Button btn_Smazat;
        private System.Windows.Forms.Button btn_NactiDB;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btn_FiltrujData;
        private System.Windows.Forms.TextBox textBox_Filter;
        private System.Windows.Forms.ComboBox comboBox_NamesFilter;
    }
}

