using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Globalization;

namespace Databazova_aplikace_1_0
{
    public partial class Form1 : Form
    {
        static BackgroundWorker bw = new BackgroundWorker(); //backgroundworker
        static bool StartZapisu = false;
        static int PocetZaznamu = 0;
        static string AdresaServeru = "mongodb://127.0.0.1:27017";
        static string NazevDatabaze = "machineStorage";
        //static string AdresaServeru = "mongodb+srv://<pass>@cluster0-p7tnn.gcp.mongodb.net/test?retryWrites=true&w=majority";
        //static string NazevDatabaze = "test";
        static string NazevKolekce = "machine";


        public Form1()
        {
            InitializeComponent();
            bw.DoWork += Bw_DoWork; //backgroundworker
        }

        private void btn_StartStop_Click(object sender, EventArgs e)
        {
            if (!bw.IsBusy)
            {
                StartZapisu = true;
                bw.RunWorkerAsync(); //run backgroundworker
                btn_StartStop.BackColor = Color.Red;
                btn_StartStop.Invoke((MethodInvoker)delegate { btn_StartStop.Text = "Stop zapisov�n� JSON"; });
            }
            else
            {
                StartZapisu = false; //stop backgroundworker
                btn_StartStop.BackColor = Color.Lime;
                btn_StartStop.Invoke((MethodInvoker)delegate { btn_StartStop.Text = "Start zapisov�n� JSON"; });
            }
        }

        [Obsolete]
        void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            while (StartZapisu)
            {

                String dirPath = @"Json_data\"; //cesta slo�ky
                if (!Directory.Exists(dirPath)) //pokud slo�ka neexistuje
                {
                    MessageBox.Show("Zji�t�na chyba, adres�� " + dirPath + " neexistuje.");
                }
                else //pokud slo�ka existuje
                {
                    string firstFileName = (new DirectoryInfo(dirPath)).GetFiles().Select(fi => fi.Name).FirstOrDefault(); //na�ti n�zev prvn�ho souboru

                    if (firstFileName != null && firstFileName != "") //pokud soubor existuje
                    {
                        JObject JsonData;
                        string JsonDataString = "";
                        try //kdy� je soubor otev�en� n�kde jinde, nelze otev��t
                        {
                            using (StreamReader TextReader = File.OpenText(dirPath + firstFileName))
                            using (JsonTextReader JsonReader = new JsonTextReader(TextReader))
                            {
                                JsonData = (JObject)JToken.ReadFrom(JsonReader);
                            }
                            JsonDataString = JsonData.ToString();
                        }
                        catch
                        {
                        }

                        //p�ipojen� k serveru
                        var dbClient = new MongoClient(AdresaServeru);
                        IMongoDatabase db = dbClient.GetDatabase(NazevDatabaze);
                        bool isMongoLive = db.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000);

                        if (JsonDataString != "" && isMongoLive) //pokud se d� p�ipojit k serveru a je co zapisovat
                        {
                            //kontrola souboru dat k z�pisu
                            JObject rss = JObject.Parse(JsonDataString);
                            string rssDate = (string)rss["Date"];
                            string rssVin = (string)rss["Vin"];
                            if (rssDate == null || rssVin == null)
                            {
                                MessageBox.Show("Soubor " + dirPath + firstFileName + " neobsahuje data Vin nebo datum s �asem. Po potvrzen� pokra�uji.");
                            }

                            //z�pis z�znamu
                            var machinery = db.GetCollection<BsonDocument>(NazevKolekce);
                            MongoDB.Bson.BsonDocument BSONDoc = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(JsonDataString);
                            var doc = new BsonDocument { BSONDoc };
                            machinery.InsertOne(doc);

                            //kontrola zapsan�ho z�znamu, p�edpoklad, �e nalezne pouze jeden z�znam se stejn�m datem a Vin ��slem
                            List<BsonDocument> docs = null;
                            if (rssDate != null && rssVin != null)
                            {
                                var builder = Builders<BsonDocument>.Filter;
                                var filter = builder.Eq("Data", rssDate) & builder.Eq("Vin", rssVin);
                                docs = machinery.Find(filter).ToList();
                            }

                            //vymaz�n� souboru
                            if ((docs != null && rssDate != null && rssVin != null) || (docs == null && rssDate == null && rssVin == null))
                            {
                                File.Delete(dirPath + firstFileName);
                                PocetZaznamu++;
                                lbl_Status.Invoke((MethodInvoker)delegate { lbl_Status.Text = "Po�et zapsan�ch z�znam�: " + PocetZaznamu; });
                            }
                            else if (docs == null && rssDate != null && rssVin != null)
                            {
                                MessageBox.Show("Soubor " + dirPath + firstFileName + " se nepovedlo zapsat. Po potvrzen� zaps�n� opakuji.");
                            }

                        }
                        else if (!isMongoLive) //pokud se ned� p�ipojit k serveru
                        {
                            MessageBox.Show("Nelze se p�ipojit k severu.");
                            btn_StartStop_Click(sender, e);
                            return;
                        }
                    }
                }
            }
        }

        private void btn_Smazat_Click(object sender, EventArgs e)
        {
            //p�ipojen� k serveru
            var dbClient = new MongoClient(AdresaServeru);
            IMongoDatabase db = dbClient.GetDatabase(NazevDatabaze);
            bool isMongoLive = db.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000);

            if (isMongoLive) //pokud se d� p�ipojit k serveru a je co zapisovat
            {
                db.DropCollectionAsync(NazevKolekce);
                MessageBox.Show("Data v datab�zi smaz�na.");
            }
            else //pokud se ned� p�ipojit k serveru
            {
                MessageBox.Show("Nelze se p�ipojit k serveru.");
            }
        }

        private void btn_NactiDB_Click(object sender, EventArgs e)
        {
            //p�ipojen� k serveru
            var dbClient = new MongoClient(AdresaServeru);
            IMongoDatabase db = dbClient.GetDatabase(NazevDatabaze);
            bool isMongoLive = db.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000);

            if (isMongoLive) //pokud se d� p�ipojit k serveru a je co zapisovat
            {
                var machinery = db.GetCollection<BsonDocument>(NazevKolekce);
                var documents = machinery.Find(new BsonDocument()).ToList();

                listBox1.Items.Clear();
                foreach (BsonDocument docc in documents)
                {
                    listBox1.Items.Add(docc.ToString());
                }
            }
            else //pokud se ned� p�ipojit k serveru
            {
                MessageBox.Show("Nelze se p�ipojit k serveru.");
            }
        }

        private void btn_FiltrujData_Click(object sender, EventArgs e)
        {
            //p�ipojen� k serveru
            var dbClient = new MongoClient(AdresaServeru);
            IMongoDatabase db = dbClient.GetDatabase(NazevDatabaze);
            bool isMongoLive = db.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000);

            if (isMongoLive) //pokud se d� p�ipojit k serveru a je co zapisovat
            {
                bool notFault = false;
                var builder = Builders<BsonDocument>.Filter;
                FilterDefinition<BsonDocument> filter = null;
                int convertedValue;
                if (comboBox_NamesFilter.Text == "GpsData")
                {
                    string intput = textBox_Filter.Text;
                    string Latitude = "";
                    string Longtitude = "";
                    int j = 0;
                    for (int i = j; i < intput.Length && intput[i] != ','; i++)
                    {
                        Latitude = Latitude + intput[i];
                        j = i;
                    }
                    j++;
                    j++;
                    for (int i = j; i < intput.Length; i++)
                    {
                        Longtitude = Longtitude + intput[i];
                    }
                    int LatitudeInt = 0;
                    int LongtitudeInt = 0;
                    int.TryParse(Latitude, out LatitudeInt);
                    int.TryParse(Longtitude, out LongtitudeInt);
                    filter = builder.Eq("GpsData", new BsonDocument { { "Latitude", LatitudeInt },{ "Longtitude", LongtitudeInt } });
                }
                else if (comboBox_NamesFilter.Text == "AccelerometerData")
                {
                    string intput = textBox_Filter.Text;
                    string AccelerationX = "";
                    string AccelerationY = "";
                    string AccelerationZ = "";
                    int j = 0;
                    for (int i = j; i < intput.Length && intput[i] != ','; i++)
                    {
                        AccelerationX = AccelerationX + intput[i];
                        j = i;
                    }
                    j++;
                    j++;
                    for (int i = j; i < intput.Length && intput[i] != ','; i++)
                    {
                        AccelerationY = AccelerationY + intput[i];
                        j = i;
                    }
                    j++;
                    j++;
                    for (int i = j; i < intput.Length; i++)
                    {
                        AccelerationZ = AccelerationZ + intput[i];
                    }
                    int AccelerationXInt = 0;
                    int AccelerationYInt = 0;
                    int AccelerationZInt = 0;
                    int.TryParse(AccelerationX, out AccelerationXInt);
                    int.TryParse(AccelerationY, out AccelerationYInt);
                    int.TryParse(AccelerationZ, out AccelerationZInt);
                    filter = builder.Eq("AccelerometerData", new BsonDocument { { "AccelerationX", AccelerationXInt }, { "AccelerationY", AccelerationYInt }, { "AccelerationZ", AccelerationZInt } });
                }
                else if (int.TryParse(textBox_Filter.Text, out convertedValue))
                {
                    filter = builder.Eq(comboBox_NamesFilter.Text, convertedValue);
                }
                else if (comboBox_NamesFilter.Text == "_id")
                {
                    try
                    {
                        filter = builder.Eq(comboBox_NamesFilter.Text, ObjectId.Parse(textBox_Filter.Text));
                    }
                    catch
                    {
                        MessageBox.Show("Identifik�tor nelze p�ev�st na ObjectId.");
                        notFault = true;
                    }
                }
                else if (comboBox_NamesFilter.Text == "createDate" || comboBox_NamesFilter.Text == "publishedAt")
                {
                    try
                    {
                        filter = builder.Gte(comboBox_NamesFilter.Text, DateTime.Parse(textBox_Filter.Text)) & builder.Lt(comboBox_NamesFilter.Text, DateTime.Parse(textBox_Filter.Text).AddHours(1));

                        //filter = builder.Eq(comboBox_NamesFilter.Text, DateTime.Parse(textBox_Filter.Text));
                        //var filter1 = builder.Eq(x => x.AsString, textBox_Filter.Text);
                        //, null, System.Globalization.DateTimeStyles.RoundtripKind
                    }
                    catch
                    {
                        MessageBox.Show("Datum typu ISO 8601 nen� spr�vn� zadan�.");
                        notFault = true; 
                    }
                }
                else if (comboBox_NamesFilter.Text == "publishedData")
                {
                    double tolerance = 0.001;
                    double double1 = 0;
                    double double2 = 0;
                    double double3 = 0;
                    bool faults = false;
                    var splittext = (textBox_Filter.Text.Remove(0,1).Remove(textBox_Filter.Text.Length-2)).Split(',');
                    double1 = Double.Parse(splittext[0], CultureInfo.InvariantCulture);
                    try
                    {
                        double2 = Double.Parse(splittext[1], CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        filter = builder.Gte("publishedData", new BsonArray { { double1 - tolerance } }) & builder.Lte("publishedData", new BsonArray { { double1 + tolerance } });
                        faults = true;
                        ;
                    }

                    try
                    {
                        double3 = Double.Parse(splittext[2], CultureInfo.InvariantCulture);
                        filter = builder.Gte("publishedData", new BsonArray { { double1 - tolerance }, { double2 - tolerance }, { double3  - tolerance } }) & builder.Lt("publishedData", new BsonArray { { double1 + tolerance }, { double2 + tolerance }, { double3 + tolerance } }); ;
                        ;
                    }
                    catch
                    {
                        if (faults == false)
                        {
                            filter = builder.Gte("publishedData", new BsonArray { { double1 - tolerance }, { double2 - tolerance } }) & builder.Lt("publishedData", new BsonArray { { double1 + tolerance }, { double2 + tolerance} });
                        }
                        
                        ;
                    }


                }
                else
                {
                    //filter = builder.Eq(comboBox_NamesFilter.Text, textBox_Filter.Text);
                    filter = Builders<BsonDocument>.Filter.Regex(comboBox_NamesFilter.Text, new BsonRegularExpression(".*" + textBox_Filter.Text + ".*"));
                    
                }

                if (filter != null)
                {
                    var machinery = db.GetCollection<BsonDocument>(NazevKolekce);
                    var docFilter = machinery.Find(filter).ToList();
                    listBox1.Items.Clear();
                    if (docFilter.Count > 0)
                    {
                        foreach (BsonDocument docc in docFilter)
                        {
                            listBox1.Items.Add(docc.ToString());
                        }
                    }
                } 
                else if (notFault == false)
                {
                    MessageBox.Show("Chyba filtru dat.");
                }
                
            }
            else //pokud se ned� p�ipojit k serveru
            {
                MessageBox.Show("Nelze se p�ipojit k serveru.");
            }
        }

        private void comboBox_NamesFilter_Click(object sender, EventArgs e)
        {
            var dbClient = new MongoClient(AdresaServeru);
            IMongoDatabase db = dbClient.GetDatabase(NazevDatabaze);
            bool isMongoLive = db.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000);
            if (isMongoLive) //pokud se d� p�ipojit k serveru a je co zapisovat
            {
                var machinery = db.GetCollection<BsonDocument>(NazevKolekce);
                var documents = machinery.Find(new BsonDocument()).ToList();
                comboBox_NamesFilter.Items.Clear();
                for (int i = 0; i < documents.FirstOrDefault().ElementCount; i++)
                {
                    var elmnt = documents.FirstOrDefault().Elements.ToList();
                    comboBox_NamesFilter.Items.Add(elmnt[i].Name);
                }
            }
            else //pokud se ned� p�ipojit k serveru
            {
                MessageBox.Show("Nelze se p�ipojit k serveru.");
            }
        }

        private void comboBox_NamesFilter_SelectedValueChanged(object sender, EventArgs e)
        {
            var dbClient = new MongoClient(AdresaServeru);
            IMongoDatabase db = dbClient.GetDatabase(NazevDatabaze);
            bool isMongoLive = db.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000);
            if (isMongoLive) //pokud se d� p�ipojit k serveru a je co zapisovat
            {
                var machinery = db.GetCollection<BsonDocument>(NazevKolekce);
                var filter = Builders<BsonDocument>.Filter.Ne(comboBox_NamesFilter.Text, "");
                var studentDocument = machinery.Find(filter).FirstOrDefault();
                ;
                if (studentDocument != null)
                {
                    textBox_Filter.Text = studentDocument.GetValue(comboBox_NamesFilter.Text).ToString();
                }
                else if (studentDocument == null)
                {
                    textBox_Filter.Text = "";
                }
            }
            else //pokud se ned� p�ipojit k serveru
            {
                MessageBox.Show("Nelze se p�ipojit k serveru.");
            }
        }

        private void textBox_Adresa_TextChanged(object sender, EventArgs e)
        {
            AdresaServeru = textBox_Adresa.Text;
            ; 
        }

        private void textBox_Databaze_TextChanged(object sender, EventArgs e)
        {
            NazevDatabaze = textBox_Databaze.Text;
        }

        private void label_kolekce_Click(object sender, EventArgs e)
        {

        }

        private void comboBox_Collection_Click(object sender, EventArgs e)
        {
            var dbClient = new MongoClient(AdresaServeru);
            IMongoDatabase db = dbClient.GetDatabase(NazevDatabaze);
            bool isMongoLive = db.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000);
            if (isMongoLive) //pokud se d� p�ipojit k serveru a je co zapisovat
            {
                var dbNames = db.ListCollectionsAsync().Result.ToListAsync<BsonDocument>().Result;
                comboBox_Collection.Items.Clear();
                for (int i = 0; i < dbNames.Count; i++)
                {
                    var elmnt = dbNames[i].Elements.ToList();
                    comboBox_Collection.Items.Add(elmnt[0].Value);
                    ;
                }
            }
            else //pokud se ned� p�ipojit k serveru
            {
                MessageBox.Show("Nelze se p�ipojit k serveru.");
            }
        }

        private void comboBox_Collection_SelectedValueChanged(object sender, EventArgs e)
        {
            NazevKolekce = comboBox_Collection.Text;
        }
    }
}
