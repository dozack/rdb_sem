﻿namespace Databazova_aplikace_1_0
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btn_Smazat = new System.Windows.Forms.Button();
            this.btn_NactiDB = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btn_FiltrujData = new System.Windows.Forms.Button();
            this.textBox_Filter = new System.Windows.Forms.TextBox();
            this.comboBox_NamesFilter = new System.Windows.Forms.ComboBox();
            this.textBox_Adresa = new System.Windows.Forms.TextBox();
            this.textBox_Databaze = new System.Windows.Forms.TextBox();
            this.label_Adresa = new System.Windows.Forms.Label();
            this.label_Databaze = new System.Windows.Forms.Label();
            this.label_kolekce = new System.Windows.Forms.Label();
            this.lbl_Status = new System.Windows.Forms.Label();
            this.btn_StartStop = new System.Windows.Forms.Button();
            this.comboBox_Collection = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btn_Smazat
            // 
            this.btn_Smazat.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Smazat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btn_Smazat.ForeColor = System.Drawing.Color.Red;
            this.btn_Smazat.Location = new System.Drawing.Point(708, 10);
            this.btn_Smazat.Name = "btn_Smazat";
            this.btn_Smazat.Size = new System.Drawing.Size(138, 34);
            this.btn_Smazat.TabIndex = 2;
            this.btn_Smazat.Text = "Smazat databázi";
            this.btn_Smazat.UseVisualStyleBackColor = false;
            this.btn_Smazat.Click += new System.EventHandler(this.btn_Smazat_Click);
            // 
            // btn_NactiDB
            // 
            this.btn_NactiDB.BackColor = System.Drawing.SystemColors.Control;
            this.btn_NactiDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_NactiDB.Location = new System.Drawing.Point(440, 11);
            this.btn_NactiDB.Name = "btn_NactiDB";
            this.btn_NactiDB.Size = new System.Drawing.Size(83, 34);
            this.btn_NactiDB.TabIndex = 4;
            this.btn_NactiDB.Text = "Načti DB";
            this.btn_NactiDB.UseVisualStyleBackColor = false;
            this.btn_NactiDB.Click += new System.EventHandler(this.btn_NactiDB_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.Location = new System.Drawing.Point(10, 53);
            this.listBox1.Name = "listBox1";
            this.listBox1.ScrollAlwaysVisible = true;
            this.listBox1.Size = new System.Drawing.Size(1340, 654);
            this.listBox1.TabIndex = 5;
            // 
            // btn_FiltrujData
            // 
            this.btn_FiltrujData.BackColor = System.Drawing.SystemColors.Control;
            this.btn_FiltrujData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_FiltrujData.Location = new System.Drawing.Point(1278, 9);
            this.btn_FiltrujData.Name = "btn_FiltrujData";
            this.btn_FiltrujData.Size = new System.Drawing.Size(74, 34);
            this.btn_FiltrujData.TabIndex = 6;
            this.btn_FiltrujData.Text = "Filtrovat";
            this.btn_FiltrujData.UseVisualStyleBackColor = false;
            this.btn_FiltrujData.Click += new System.EventHandler(this.btn_FiltrujData_Click);
            // 
            // textBox_Filter
            // 
            this.textBox_Filter.Location = new System.Drawing.Point(986, 17);
            this.textBox_Filter.Multiline = true;
            this.textBox_Filter.Name = "textBox_Filter";
            this.textBox_Filter.Size = new System.Drawing.Size(290, 20);
            this.textBox_Filter.TabIndex = 7;
            this.textBox_Filter.Text = "<- nejdřív vyberte hledaný tag";
            // 
            // comboBox_NamesFilter
            // 
            this.comboBox_NamesFilter.AccessibleDescription = "";
            this.comboBox_NamesFilter.AccessibleName = "";
            this.comboBox_NamesFilter.DisplayMember = "fgh";
            this.comboBox_NamesFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_NamesFilter.FormattingEnabled = true;
            this.comboBox_NamesFilter.Location = new System.Drawing.Point(852, 16);
            this.comboBox_NamesFilter.Name = "comboBox_NamesFilter";
            this.comboBox_NamesFilter.Size = new System.Drawing.Size(131, 21);
            this.comboBox_NamesFilter.Sorted = true;
            this.comboBox_NamesFilter.TabIndex = 9;
            this.comboBox_NamesFilter.Tag = "";
            this.comboBox_NamesFilter.SelectedValueChanged += new System.EventHandler(this.comboBox_NamesFilter_SelectedValueChanged);
            this.comboBox_NamesFilter.Click += new System.EventHandler(this.comboBox_NamesFilter_Click);
            // 
            // textBox_Adresa
            // 
            this.textBox_Adresa.Location = new System.Drawing.Point(96, 5);
            this.textBox_Adresa.Name = "textBox_Adresa";
            this.textBox_Adresa.Size = new System.Drawing.Size(338, 20);
            this.textBox_Adresa.TabIndex = 10;
            this.textBox_Adresa.Text = "mongodb://127.0.0.1:27017";
            this.textBox_Adresa.TextChanged += new System.EventHandler(this.textBox_Adresa_TextChanged);
            // 
            // textBox_Databaze
            // 
            this.textBox_Databaze.Location = new System.Drawing.Point(96, 28);
            this.textBox_Databaze.Name = "textBox_Databaze";
            this.textBox_Databaze.Size = new System.Drawing.Size(128, 20);
            this.textBox_Databaze.TabIndex = 11;
            this.textBox_Databaze.Text = "machineStorage";
            this.textBox_Databaze.TextChanged += new System.EventHandler(this.textBox_Databaze_TextChanged);
            // 
            // label_Adresa
            // 
            this.label_Adresa.AutoSize = true;
            this.label_Adresa.Location = new System.Drawing.Point(6, 8);
            this.label_Adresa.Name = "label_Adresa";
            this.label_Adresa.Size = new System.Drawing.Size(81, 13);
            this.label_Adresa.TabIndex = 13;
            this.label_Adresa.Text = "Adresa serveru:";
            // 
            // label_Databaze
            // 
            this.label_Databaze.AutoSize = true;
            this.label_Databaze.Location = new System.Drawing.Point(6, 32);
            this.label_Databaze.Name = "label_Databaze";
            this.label_Databaze.Size = new System.Drawing.Size(88, 13);
            this.label_Databaze.TabIndex = 14;
            this.label_Databaze.Text = "Název databáze:";
            // 
            // label_kolekce
            // 
            this.label_kolekce.AutoSize = true;
            this.label_kolekce.Location = new System.Drawing.Point(226, 32);
            this.label_kolekce.Name = "label_kolekce";
            this.label_kolekce.Size = new System.Drawing.Size(82, 13);
            this.label_kolekce.TabIndex = 15;
            this.label_kolekce.Text = "Název kolekce:";
            this.label_kolekce.Click += new System.EventHandler(this.label_kolekce_Click);
            // 
            // lbl_Status
            // 
            this.lbl_Status.AutoSize = true;
            this.lbl_Status.Location = new System.Drawing.Point(527, 35);
            this.lbl_Status.Name = "lbl_Status";
            this.lbl_Status.Size = new System.Drawing.Size(146, 13);
            this.lbl_Status.TabIndex = 16;
            this.lbl_Status.Text = "Počet zapsaných záznamů: 0";
            // 
            // btn_StartStop
            // 
            this.btn_StartStop.BackColor = System.Drawing.Color.Lime;
            this.btn_StartStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btn_StartStop.Location = new System.Drawing.Point(527, 1);
            this.btn_StartStop.Name = "btn_StartStop";
            this.btn_StartStop.Size = new System.Drawing.Size(177, 33);
            this.btn_StartStop.TabIndex = 17;
            this.btn_StartStop.Text = "Start zapisování JSON";
            this.btn_StartStop.UseVisualStyleBackColor = false;
            this.btn_StartStop.Click += new System.EventHandler(this.btn_StartStop_Click);
            // 
            // comboBox_Collection
            // 
            this.comboBox_Collection.AccessibleName = "";
            this.comboBox_Collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Collection.FormattingEnabled = true;
            this.comboBox_Collection.Location = new System.Drawing.Point(308, 28);
            this.comboBox_Collection.Name = "comboBox_Collection";
            this.comboBox_Collection.Size = new System.Drawing.Size(126, 21);
            this.comboBox_Collection.Sorted = true;
            this.comboBox_Collection.TabIndex = 18;
            this.comboBox_Collection.Tag = "";
            this.comboBox_Collection.SelectedValueChanged += new System.EventHandler(this.comboBox_Collection_SelectedValueChanged);
            this.comboBox_Collection.Click += new System.EventHandler(this.comboBox_Collection_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1355, 717);
            this.Controls.Add(this.comboBox_Collection);
            this.Controls.Add(this.btn_StartStop);
            this.Controls.Add(this.lbl_Status);
            this.Controls.Add(this.label_kolekce);
            this.Controls.Add(this.label_Databaze);
            this.Controls.Add(this.label_Adresa);
            this.Controls.Add(this.textBox_Databaze);
            this.Controls.Add(this.textBox_Adresa);
            this.Controls.Add(this.comboBox_NamesFilter);
            this.Controls.Add(this.textBox_Filter);
            this.Controls.Add(this.btn_FiltrujData);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btn_NactiDB);
            this.Controls.Add(this.btn_Smazat);
            this.Name = "Form1";
            this.Text = "Mongo database browser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btn_Smazat;
        private System.Windows.Forms.Button btn_NactiDB;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btn_FiltrujData;
        private System.Windows.Forms.TextBox textBox_Filter;
        private System.Windows.Forms.ComboBox comboBox_NamesFilter;
        private System.Windows.Forms.TextBox textBox_Adresa;
        private System.Windows.Forms.TextBox textBox_Databaze;
        private System.Windows.Forms.Label label_Adresa;
        private System.Windows.Forms.Label label_Databaze;
        private System.Windows.Forms.Label label_kolekce;
        private System.Windows.Forms.Label lbl_Status;
        private System.Windows.Forms.Button btn_StartStop;
        private System.Windows.Forms.ComboBox comboBox_Collection;
    }
}

