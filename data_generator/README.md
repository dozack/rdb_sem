# MongoDB data generator
Simulátor zemědělských strojů vyvinutý v rámci vypracování semestrální práce na kurz Řízení databází  
Autor: Zoltán Dolenský

Pro snadné vyzkoušení je ve složce _RELEASE připraven exe soubor, který je potřeba spouštět v konzoli:

`MongoManager.exe <NODE_COUNT> <DROP_DB>`

- NODE_COUNT udává počet vytvořených strojů instanci generátoru
- DROP_DB `-y` smaže předchozí data v DB, volitelné (pokud nechci, nepsat) 

## Komponenty

- SnumGen
- SensorSimulator
- NodeSimulator
- MongoManager

Podporované typy senzorů:

- GPS  - `GpsSensor`
- napětí - `VoltageSensor`
- teplota - `TemperatureSensor`
- Akcelerace - `AccelerationSensor`
- mechanické napětí - `TensionSensor`

