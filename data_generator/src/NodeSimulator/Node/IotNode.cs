﻿using NodeSimulator.Configuration;
using NodeSimulator.NameEngine;
using SensorSimulator.API;
using SnumGen;
using System;
using System.Linq;
using System.Timers;

namespace NodeSimulator.Node
{
    /// <summary>
    /// Iot unit simulation - uses Sensor API to simulate node behavior
    /// </summary>
    public class IotNode
    {
        /// <summary>
        /// Random number generator
        /// </summary>
        readonly Random rnd = new Random();

        /// <summary>
        /// Timer for publishing measurements
        /// </summary>
        private readonly Timer PublishTimer;

        /// <summary>
        /// Timer for possible switching status or changing confguration
        /// </summary>
        private readonly Timer ShiftTimer;

        /// <summary>
        /// Serial number of IOT node
        /// </summary>
        public string SerialNumber { get; }

        /// <summary>
        /// Actual status of node
        /// </summary>
        public NodeStatus Status { get; private set; } = NodeStatus.Online;

        /// <summary>
        /// MUST HAVE - Actual configuration
        /// </summary>
        public NodeConfiguration Configuration { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="InitialConfig">Initial configuration of unit</param>
        public IotNode(NodeConfiguration InitialConfig)
        {
            // Initial argument check
            if (InitialConfig == null) throw new Exception("Initial configuration null.");
            if (InitialConfig.PublishInterval == 0) throw new Exception("Publish interval not set.");

            // Assign configuration
            Configuration = InitialConfig;

            // Message publish depending of configuration 
            PublishTimer = new Timer(Configuration.PublishInterval)
            {
                AutoReset = true,
            };
            // Possible status or config change every 2-8 hours
            ShiftTimer = new Timer(rnd.Next(2, 9) * 3600000)
            //ShiftTimer = new Timer(rnd.Next(2, 9) * 600)
            {
                AutoReset = true,
            };

            // Assign event handlers to timers
            PublishTimer.Elapsed += PublishTimer_Elapsed;
            ShiftTimer.Elapsed += ShiftTimer_Elapsed;

            SerialNumber = SerialNumberGenerator.GetUnique(rnd.Next(20, 31));
        }

        /// <summary>
        /// Begin simulation of IOT node by firing up the timers
        /// </summary>
        public void Begin()
        {
            PublishTimer.Start();
            ShiftTimer.Start();
        }

        /// <summary>
        /// Shift change timer overflow event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShiftTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            // Stop timer
            ShiftTimer.Stop();

            // 10% chance of changing status
            if (rnd.NextDouble() < 0.1)
            {
                // Get new random status
                var values = Enum.GetValues(typeof(NodeStatus));
                Status = (NodeStatus)values.GetValue(rnd.Next(values.Length));

                // Notify higher layer of status change
                NotifyStatusChanged(Status);
            }
            // 1% chance to change configuration - will be lowered if happens too often
            if (rnd.NextDouble() < 0.01)
            {
                // Generate new configuration with random name
                NodeConfiguration config = new NodeConfiguration(Name.GetRandomUnique());

                // First remove old name and serial numbers from unique list
                Name.RemoveFromUniques(Configuration.NodeName);
                foreach (var sensor in Configuration.Sensors)
                {
                    SerialNumberGenerator.Dump(sensor.SerialNumber);
                }
                SerialNumberGenerator.Dump(Configuration.ConfigId);

                // Then assign new config
                Configuration = config;

                // Notify higher layer of config change
                NotifyConfigChanged(Configuration);
            }

            // Start timer
            ShiftTimer.Start();
        }

        /// <summary>
        /// Publish timer overflow event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PublishTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            // Stop timer
            PublishTimer.Stop();

            // Do not publish if node is offline
            if (Status == NodeStatus.Online)
            {
                // Get measurement from each sensor and publish
                foreach (var sensor in Configuration.Sensors)
                {
                    SensorData data = sensor.Measure();
                    // TODO: Passing collection instead of every single object to reduce event load on app
                    Publish(data);
                }
            }

            // Start timer
            PublishTimer.Start();
        }

        private string RandomSerialNumber(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[rnd.Next(s.Length)]).ToArray());
        }

        #region PUBLISH_EVENT
        /// <summary>
        /// Delegate for publishing messages from node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="dataPublished"></param>
        public delegate void PublishData(IotNode sender, SensorData dataPublished);

        /// <summary>
        /// Event of published message
        /// </summary>
        public event PublishData OnPublish;

        /// <summary>
        /// Method for invoking publish event
        /// </summary>
        /// <param name="data"></param>
        protected virtual void Publish(SensorData data) { OnPublish?.Invoke(this, data); }
        #endregion

        #region CONFIG_UPDATE_EVENT
        /// <summary>
        /// Delegate for notification of configuration change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="configSet"></param>
        public delegate void UpdateConfig(IotNode sender, NodeConfiguration configSet);

        /// <summary>
        /// Event of configuration updated
        /// </summary>
        public event UpdateConfig OnConfigUpdate;

        /// <summary>
        /// Method for invoking notification of config update
        /// </summary>
        /// <param name="config"></param>
        protected virtual void NotifyConfigChanged(NodeConfiguration config) { OnConfigUpdate?.Invoke(this, config); }
        #endregion

        #region STATUS_CHANGE_EVENT
        /// <summary>
        /// Delegate for notification of status change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="statusSet"></param>
        public delegate void ChangeStatus(IotNode sender, NodeStatus statusSet);

        /// <summary>
        /// Event of status change
        /// </summary>
        public event ChangeStatus OnStatusChanged;

        /// <summary>
        /// Method for invoking status change event
        /// </summary>
        /// <param name="status"></param>
        protected virtual void NotifyStatusChanged(NodeStatus status) { OnStatusChanged?.Invoke(this, status); }
        #endregion
    }
}
