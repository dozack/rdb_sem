﻿using NodeSimulator.Configuration;
using NodeSimulator.NameEngine;
using NodeSimulator.Node;
using SensorSimulator.API;
using System;
using System.Collections.ObjectModel;

namespace NodeSimulator
{
    /// <summary>
    /// Some rubbish for testing implementation
    /// </summary>
    class NodeTest
    {
        static ObservableCollection<IotNode> nodes = new ObservableCollection<IotNode>();


        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                IotNode node = new IotNode(new NodeConfiguration(Name.GetRandomUnique()));

                node.OnConfigUpdate += Node_OnConfigUpdate;
                node.OnStatusChanged += Node_OnStatusChanged;
                node.OnPublish += Node_OnPublish;

                nodes.Add(node);
            }

            foreach (var node in nodes)
            {
                node.Begin();
            }

            while (true)
            {

            }
        }

        private static void Node_OnPublish(IotNode sender, SensorData dataPublished)
        {
            Console.Write("Node {0} published data from sensor {1}: ", sender.SerialNumber, dataPublished.SensorId);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("[");
            for (int i = 0; i < dataPublished.Data.Length; i++)
            {
                if (i > 0)
                {
                    Console.Write("|");
                }
                Console.Write(dataPublished.Data[i]);
            }
            Console.WriteLine("]");
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        private static void Node_OnStatusChanged(IotNode sender, NodeStatus statusSet)
        {
            Console.WriteLine("Node changed status...");
        }

        private static void Node_OnConfigUpdate(IotNode sender, NodeConfiguration configSet)
        {
            Console.WriteLine("Node changed configuration...");
        }
    }
}
