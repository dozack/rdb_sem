﻿using System;

namespace NodeSimulator.NameEngine
{
    /// <summary>
    /// Noun storage class
    /// </summary>
    public static class Noun
    {
        /// <summary>
        /// Noun collection - feel free to expand
        /// </summary>
        private static string[] nounList =
        {
            "hair",
            "insurance",
            "grocery",
            "studio",
            "bird",
            "membership",
            "improvement",
            "childhood",
            "attitude",
            "person",
            "description",
            "library",
            "salad",
            "lady",
            "midnight",
            "confusion",
            "language",
            "flight",
            "buyer",
            "boyfriend",
            "equipment",
            "explanation",
            "secretary",
            "committee",
            "bathroom",
            "lake",
            "diamond",
            "article",
            "news",
            "distribution",
            "two",
            "chapter",
            "love",
            "photo",
            "employment",
            "fishing",
            "employer",
            "family",
            "possibility",
            "literature",
            "examination",
            "death",
            "clothes",
            "song",
            "investment",
            "chemistry",
            "historian",
            "cookie",
            "football",
            "housing",
        };

        /// <summary>
        /// Get random noun
        /// </summary>
        /// <returns></returns>
        public static string GetRandom()
        {
            Random rnd = new Random();

            string name = nounList[rnd.Next(nounList.Length)];

            return Char.ToUpperInvariant(name[0]) + name.Substring(1);
        }
    }
}
