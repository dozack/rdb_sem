﻿using System.Collections.ObjectModel;

namespace NodeSimulator.NameEngine
{
    /// <summary>
    /// Random name generator class
    /// </summary>
    public static class Name
    {
        /// <summary>
        /// Collection of unique names
        /// </summary>
        private static ObservableCollection<string> names = new ObservableCollection<string>();

        /// <summary>
        /// Get random name - UNIQUENESS NOT SECURED
        /// </summary>
        /// <returns>Two part name consisting of adjective and noun</returns>
        public static string GetRandom()
        {
            return Adjective.GetRandom() + " " + Noun.GetRandom();
        }

        /// <summary>
        /// Get random unique name
        /// TODO: handle possible permutations overflow
        /// </summary>
        /// <returns>Name unique for this instance</returns>
        public static string GetRandomUnique()
        {
            string tmpName;
            while (names.Contains((tmpName = GetRandom())))
            {
            }
            names.Add(tmpName);
            return tmpName;
        }

        /// <summary>
        /// Remove name from unique list
        /// </summary>
        /// <param name="NameToRemove">Name to remove</param>
        public static void RemoveFromUniques(string NameToRemove)
        {
            if (names.Contains(NameToRemove))
            {
                names.Remove(NameToRemove);
            }
        }
    }
}
