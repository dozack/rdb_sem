﻿using System;

namespace NodeSimulator.NameEngine
{
    /// <summary>
    /// Adjective storage class
    /// </summary>
    public static class Adjective
    {
        /// <summary>
        /// Adjective collection - add new there
        /// </summary>
        private static string[] adjList = {
            "interesting",
            "well-made",
            "three",
            "abounding",
            "horrible",
            "big",
            "romantic",
            "delirious",
            "unwieldy",
            "selective",
            "puzzling",
            "boundless",
            "psychedelic",
            "exciting",
            "billowy",
            "equable",
            "tasteful",
            "ultra",
            "fine",
            "assorted",
            "selfish",
            "unusual",
            "fascinated",
            "fair",
            "mighty",
            "wet",
            "kindly",
            "stupendous",
            "disgusted",
            "scintillating",
            "neighborly",
            "ossified",
            "neat",
            "bizarre",
            "obtainable",
            "stiff",
            "ratty",
            "spotless",
            "same",
            "marvelous",
            "feeble",
            "questionable",
            "rabid",
            "lazy",
            "galactic",
            "simple",
            "sloppy",
            "fluffy",
            "encouraging",
            "puny",
        };

        /// <summary>
        /// Get random adjective from collection
        /// </summary>
        /// <returns></returns>
        public static string GetRandom()
        {
            Random rnd = new Random();

            string name = adjList[rnd.Next(adjList.Length)];

            return Char.ToUpperInvariant(name[0]) + name.Substring(1);
        }
    }
}
