﻿using SensorSimulator.API;
using SnumGen;
using System;
using System.Collections.ObjectModel;

namespace NodeSimulator.Configuration
{
    public class NodeConfiguration
    {
        /// <summary>
        /// Configuration ID
        /// </summary>
        public string ConfigId { get; }

        /// <summary>
        /// Timestamp of configuration creation
        /// </summary>
        public DateTime CreatedAt { get; }

        /// <summary>
        /// Assigned name of node
        /// </summary>
        public string NodeName { get; }

        /// <summary>
        /// Interval of publishing messages
        /// </summary>
        public int PublishInterval { get; }

        /// <summary>
        /// Collection of sensors connected to node
        /// </summary>
        public ObservableCollection<Sensor> Sensors { get; } = new ObservableCollection<Sensor>();

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Name">Assign name to node</param>
        public NodeConfiguration(string Name)
        {
            NodeName = Name;
            CreatedAt = DateTime.Now;
            Random rnd = new Random();

            // Random publish interval of 20 - 60 seconds
            PublishInterval = rnd.Next(20, 61) * 1000;

            // Random set of sensors in range of 3 - 10
            RandomSensorSet(rnd.Next(3, 11));

            ConfigId = SerialNumberGenerator.GetUnique(15);
        }

        /// <summary>
        /// Create random set of sensors - only unique sensor is GPS (no need for two of them on machine)
        /// </summary>
        /// <param name="count">number of sensors</param>
        private void RandomSensorSet(int count)
        {
            Random rnd = new Random();

            // GPS already in collection flag
            bool hasGPS = false;

            var vals = Enum.GetValues(typeof(SensorType));

            for (int i = 0; i < count; i++)
            {
                Sensor sensor = new Sensor((SensorType)vals.GetValue(rnd.Next(vals.Length)));

                // If created sensor is GPS, add to collection and raise flag
                if (sensor.Type == SensorType.GPS && hasGPS == false)
                {
                    hasGPS = true;
                    Sensors.Add(sensor);
                }
                else if (sensor.Type != SensorType.GPS)
                {
                    Sensors.Add(sensor);
                }
            }
        }
    }
}
