﻿using SensorSimulator.Types;

namespace SensorSimulator.Interface
{
    /// <summary>
    /// Interface for sensor simulation support
    /// </summary>
    public interface ISensor
    {
        /// <summary>
        /// Sensor serial number
        /// </summary>
        public string SerialNumber { get; }

        /// <summary>
        /// Sensor type
        /// </summary>
        public SensorType Type { get; }

        /// <summary>
        /// Read measurement from sensor that implemented ISensor
        /// </summary>
        /// <returns>Data object containing sensor measurements</returns>
        public object Measure();
    }
}
