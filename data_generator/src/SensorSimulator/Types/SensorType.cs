﻿namespace SensorSimulator.Types
{
    /// <summary>
    /// Enumeration of supported sensor types
    /// </summary>
    public enum SensorType
    {
        Temperature,
        Voltage,
        Tension,
        GPS,
        Acceleration
    }
}
