﻿using SensorSimulator.Interface;
using SensorSimulator.Types;
using SnumGen;
using System;

namespace SensorSimulator.Temperature
{
    /// <summary>
    /// Sensor simulating temperature measurements
    /// </summary>
    public class TemperatureSensor : ISensor
    {
        /// <summary>
        /// Serial number
        /// </summary>
        public string SerialNumber { get; }

        /// <summary>
        /// Sensor type
        /// </summary>
        public SensorType Type { get; } = SensorType.Temperature;

        /// <summary>
        /// Random number generator
        /// </summary>
        private readonly Random rnd;

        /// <summary>
        /// Minimal measurement value of sensor
        /// </summary>
        private double? MinValue;

        /// <summary>
        /// Maximal measurement value of sensor
        /// </summary>
        private double? MaxValue;

        /// <summary>
        /// Actual measurement
        /// </summary>
        public double? ActualValue { get; private set; }

        /// <summary>
        /// Constructor with boundaries set
        /// </summary>
        /// <param name="Snum">Serial number</param>
        /// <param name="InitialValue">Initial value</param>
        /// <param name="UpperBoundary">Maximum value</param>
        /// <param name="LowerBoundary">Minimum value</param>
        public TemperatureSensor(double InitialValue, double UpperBoundary, double LowerBoundary) : this(InitialValue)
        {
            if (UpperBoundary < LowerBoundary) throw new Exception("Invalid boundary set (Upper < Lower).");

            MinValue = LowerBoundary;
            MaxValue = UpperBoundary;
        }

        /// <summary>
        /// Default onstructor with fixed boundaries
        /// </summary>
        /// <param name="Snum">Serial number</param>
        /// <param name="InitialValue">Initial value</param>
        public TemperatureSensor(double InitialValue)
        {
            rnd = new Random();

            ActualValue = InitialValue;
            MinValue = ActualValue - 10.0;
            MaxValue = ActualValue + 10.0;

            SerialNumber = SerialNumberGenerator.GetUnique(rnd.Next(10, 21));
        }

        /// <summary>
        /// Trigger measurement
        /// </summary>
        /// <returns>Measured value</returns>
        public object Measure()
        {
            if (ActualValue == null)
            {
                return null;
            }

            ActualValue += rnd.NextDouble() * (0.2 + 0.2) - 0.2;

            if (ActualValue > MaxValue)
            {
                ActualValue = MaxValue;
            }
            else if (ActualValue < MinValue)
            {
                ActualValue = MaxValue;
            }
            return Math.Round((double)ActualValue, 2);
        }
    }
}
