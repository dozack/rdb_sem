﻿namespace SensorSimulator.Acceleration
{
    /// <summary>
    /// Structure for transfering acceleration data
    /// </summary>
    public class AccelerationData
    {
        public double? AccelerationX { get; set; }
        public double? AccelerationY { get; set; }
        public double? AccelerationZ { get; set; }
    }
}
