﻿using SensorSimulator.Interface;
using SensorSimulator.Types;
using SnumGen;
using System;

namespace SensorSimulator.Acceleration
{
    public class AccelerationSensor : ISensor
    {
        /// <summary>
        /// Serial number
        /// </summary>
        public string SerialNumber { get; }

        /// <summary>
        /// Sensor type
        /// </summary>
        public SensorType Type { get; } = SensorType.Acceleration;

        /// <summary>
        /// Random number generator
        /// </summary>
        private readonly Random rnd;

        /// <summary>
        /// Minimal measurement value of sensor
        /// </summary>
        private double? MinValue;

        /// <summary>
        /// Maximal measurement value of sensor
        /// </summary>
        private double? MaxValue;

        /// <summary>
        /// Actual measurement
        /// </summary>
        public AccelerationData ActualValue { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Snum">Serial number</param>
        /// <param name="InitialValue">Initial value</param>
        public AccelerationSensor(AccelerationData InitialValue)
        {
            rnd = new Random();

            ActualValue = new AccelerationData { AccelerationX = InitialValue.AccelerationX, AccelerationY = InitialValue.AccelerationY, AccelerationZ = InitialValue.AccelerationZ };
            MinValue = -4.0;
            MaxValue = 4.0;

            SerialNumber = SerialNumberGenerator.GetUnique(rnd.Next(10, 21));
        }

        /// <summary>
        /// Trigger measurement
        /// </summary>
        /// <returns>Object containing measured values</returns>
        public object Measure()
        {
            if (ActualValue == null)
            {
                return null;
            }

            ActualValue.AccelerationX += rnd.NextDouble() * (0.05 + 0.05) - 0.05;
            ActualValue.AccelerationY += rnd.NextDouble() * (0.05 + 0.05) - 0.05;
            ActualValue.AccelerationZ += rnd.NextDouble() * (0.005 + 0.005) - 0.005;

            //TODO: Refactor this mess

            if (ActualValue.AccelerationX > MaxValue)
            {
                ActualValue.AccelerationX = MaxValue;
            }
            else if (ActualValue.AccelerationY > MaxValue)
            {
                ActualValue.AccelerationY = MaxValue;
            }
            else if (ActualValue.AccelerationZ > MaxValue)
            {
                ActualValue.AccelerationZ = MaxValue;
            }
            else if (ActualValue.AccelerationX < MinValue)
            {
                ActualValue.AccelerationX = MinValue;
            }
            else if (ActualValue.AccelerationY < MinValue)
            {
                ActualValue.AccelerationY = MinValue;
            }
            else if (ActualValue.AccelerationZ < MinValue)
            {
                ActualValue.AccelerationZ = MinValue;
            }

            ActualValue.AccelerationX = Math.Round((double)ActualValue.AccelerationX, 4);
            ActualValue.AccelerationY = Math.Round((double)ActualValue.AccelerationY, 4);
            ActualValue.AccelerationZ = Math.Round((double)ActualValue.AccelerationZ, 4);

            return ActualValue;
        }
    }
}
