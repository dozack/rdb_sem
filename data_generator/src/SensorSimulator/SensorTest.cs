﻿using SensorSimulator.Acceleration;
using SensorSimulator.GPS;
using SensorSimulator.Interface;
using SensorSimulator.Temperature;
using SensorSimulator.Tension;
using SensorSimulator.Types;
using SensorSimulator.Voltage;
using System;
using System.Collections.ObjectModel;
using System.Timers;

namespace SensorSimulator
{
    /// <summary>
    /// Just some rubbish for testing sensor classes
    /// </summary>
    class SensorTest
    {
        public static Random rnd = new Random();
        public static Timer tim = new Timer();

        public static ObservableCollection<ISensor> Sensors;

        static void Main()
        {
            tim.Interval = rnd.NextDouble() * 10000;
            tim.AutoReset = true;
            tim.Elapsed += Tim_Elapsed;

            Sensors = new ObservableCollection<ISensor>();

            for (int i = 0; i < 30; i++)
            {
                var arr = Enum.GetValues(typeof(SensorType));
                SensorType rndType = (SensorType)arr.GetValue(rnd.Next(arr.Length));

                switch (rndType)
                {
                    case SensorType.Temperature:
                        Sensors.Add(new TemperatureSensor(rnd.NextDouble() * (100 + 40) - 40));
                        break;
                    case SensorType.Voltage:
                        Sensors.Add(new VoltageSensor(rnd.NextDouble() * (26 - 22) + 22));
                        break;
                    case SensorType.Tension:
                        Sensors.Add(new TensionSensor(rnd.NextDouble() * (40000 - 0) + 0));
                        break;
                    case SensorType.GPS:
                        GpsData iniGPS = new GpsData { Latitude = rnd.NextDouble() * (90 + 90) - 90, Longtitude = rnd.NextDouble() * (180 + 180) - 180 };
                        Sensors.Add(new GpsSensor(iniGPS));
                        break;
                    case SensorType.Acceleration:
                        AccelerationData iniAcceleration = new AccelerationData { AccelerationX = rnd.NextDouble() * (4 + 4) - 4, AccelerationY = rnd.NextDouble() * (4 + 4) - 4, AccelerationZ = rnd.NextDouble() * (1.0 + 1.0) - 1.0 };
                        Sensors.Add(new AccelerationSensor(iniAcceleration));
                        break;
                }
            }

            tim.Start();

            Console.WriteLine("Logging readings from {0} sensors..", Sensors.Count);
            Console.WriteLine("");

            Console.ReadKey();
        }

        private static void Tim_Elapsed(object sender, ElapsedEventArgs e)
        {
            tim.Stop();

            Console.Clear();
            Console.WriteLine("Logging readings from {0} sensors..", Sensors.Count);
            Console.WriteLine("");

            foreach (var sensor in Sensors)
            {

                object val = sensor.Measure();

                switch (val)
                {
                    case double _:
                        Console.WriteLine("#{0}: {1}", sensor.SerialNumber, val.ToString());
                        break;
                    case GpsData g:
                        Console.WriteLine("#{0}: {1}|{2}", sensor.SerialNumber, g.Latitude.ToString(), g.Longtitude.ToString());
                        break;
                    case AccelerationData a:
                        Console.WriteLine("#{0}: {1}|{2}|{3}", sensor.SerialNumber, a.AccelerationX.ToString(), a.AccelerationY.ToString(), a.AccelerationZ.ToString());
                        break;
                }

                // I optimized all of this code into 10 lines haha

                /*if (sensor is TemperatureSensor)
                {
                    TemperatureSensor _sensor = sensor as TemperatureSensor;
                    try
                    {
                        double? val = (double?)_sensor.Measure();
                        if (val == null) throw new Exception("Measured value null, sensor not initialized");
                        Console.WriteLine("#{0}-{1}: {2} °C", _sensor.SerialNumber, ((int)_sensor.Type).ToString(), val.ToString());
                        Console.WriteLine("--------------");
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    
                }
                else if (sensor is VoltageSensor)
                {
                    VoltageSensor _sensor = sensor as VoltageSensor;
                    try
                    {
                        double? val = (double?)_sensor.Measure();
                        if (val == null) throw new Exception("Measured value null, sensor not initialized");
                        Console.WriteLine("#{0}-{1}: {2} V", _sensor.SerialNumber, ((int)_sensor.Type).ToString(), val.ToString());
                        Console.WriteLine("--------------");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                else if (sensor is TensionSensor)
                {
                    TensionSensor _sensor = sensor as TensionSensor;
                    try
                    {
                        double? val = (double?)_sensor.Measure();
                        if (val == null) throw new Exception("Measured value null, sensor not initialized");
                        Console.WriteLine("#{0}-{1}: {2} N", _sensor.SerialNumber, ((int)_sensor.Type).ToString(), val.ToString());
                        Console.WriteLine("--------------");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                else if (sensor is GpsSensor)
                {
                    GpsSensor _sensor = sensor as GpsSensor;
                    try
                    {
                        GpsData val = (GpsData)_sensor.Measure();
                        if (val == null) throw new Exception("Measured value null, sensor not initialized");
                        Console.WriteLine("#{0}-{1}: LA:{2} | LO:{3} °", _sensor.SerialNumber, ((int)_sensor.Type).ToString(), val.Latitude.ToString(), val.Longtitude.ToString());
                        Console.WriteLine("--------------");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                else if (sensor is AccelerationSensor)
                {
                    AccelerationSensor _sensor = sensor as AccelerationSensor;
                    try
                    {
                        AccelerationData val = (AccelerationData)_sensor.Measure();
                        if (val == null) throw new Exception("Measured value null, sensor not initialized");
                        Console.WriteLine("#{0}-{1}: X:{2} | Y:{3} | Z:{4} G", _sensor.SerialNumber, ((int)_sensor.Type).ToString(), val.AccelerationX.ToString(), val.AccelerationY.ToString(), val.AccelerationZ.ToString());
                        Console.WriteLine("--------------");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }*/
            }

            tim.Start();
        }
    }
}
