﻿using SensorSimulator.Interface;
using SensorSimulator.Types;
using SnumGen;
using System;

namespace SensorSimulator.GPS
{
    /// <summary>
    /// Sensor simulating GPS location reporting
    /// </summary>
    public class GpsSensor : ISensor
    {
        /// <summary>
        /// Serial number
        /// </summary>
        public string SerialNumber { get; }

        /// <summary>
        /// Sensor type
        /// </summary>
        public SensorType Type { get; } = SensorType.GPS;

        /// <summary>
        /// Random number generator
        /// </summary>
        private readonly Random rnd;

        /// <summary>
        /// Lower boundary of measurements
        /// </summary>
        private GpsData MinValue;

        /// <summary>
        /// Upper boundary of measurements
        /// </summary>
        private GpsData MaxValue;

        /// <summary>
        /// Actual measurement
        /// </summary>
        public GpsData ActualValue { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Snum">Serial number</param>
        /// <param name="InitialValue">Initial location</param>
        public GpsSensor(GpsData InitialValue)
        {
            rnd = new Random();

            ActualValue = new GpsData { Latitude = InitialValue.Latitude, Longtitude = InitialValue.Longtitude };
            MinValue = new GpsData { Latitude = -90.0, Longtitude = -180.0 };
            MaxValue = new GpsData { Latitude = 90.0, Longtitude = 180.0 };

            SerialNumber = SerialNumberGenerator.GetUnique(rnd.Next(10, 21));
        }

        /// <summary>
        /// Trigger measurement
        /// </summary>
        /// <returns>Object containing measured values</returns>
        public object Measure()
        {
            if (ActualValue == null)
            {
                return null;
            }

            ActualValue.Latitude += rnd.NextDouble() * (0.005 + 0.005) - 0.005;
            ActualValue.Longtitude += rnd.NextDouble() * (0.005 + 0.005) - 0.005;

            //TODO: Refactor this mess

            if (ActualValue.Latitude > MaxValue.Latitude)
            {
                ActualValue.Latitude = MaxValue.Latitude;
            }
            else if (ActualValue.Longtitude > MaxValue.Longtitude)
            {
                ActualValue.Longtitude = MaxValue.Longtitude;
            }

            else if (ActualValue.Latitude < MinValue.Latitude)
            {
                ActualValue.Latitude = MinValue.Latitude;
            }
            else if (ActualValue.Longtitude < MinValue.Longtitude)
            {
                ActualValue.Longtitude = MinValue.Longtitude;
            }

            ActualValue.Latitude = Math.Round((double)ActualValue.Latitude, 6);
            ActualValue.Longtitude = Math.Round((double)ActualValue.Longtitude, 6);

            return ActualValue;
        }
    }
}
