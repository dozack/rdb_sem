﻿namespace SensorSimulator.GPS
{
    /// <summary>
    /// Structure for transfering GPS coordinates
    /// </summary>
    public class GpsData
    {
        public double? Latitude { get; set; }
        public double? Longtitude { get; set; }
    }
}
