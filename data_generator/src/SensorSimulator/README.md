# SensorSimulator 

Pro vývoj dalších typů slouží rozhraní `ISensor`.

### API

Vytvoření instance senzoru s náhodnou počáteční hodnotou:

```csharp
Sensor sensor = new Sensor(SensorType.Voltage);
```

Reinicializace senzoru na novou náhodnou hodnotu:

```csharp
sensor.Reinitialize();
```

Reinicializace senzoru s nastavením rozsahu měřených hodnot. Aktuální hodnota je nastavena na průměr hodnot rozsahu:

```csharp
// Reinitialize(Max, Min) => Actual = (Max+Min)/2
sensor.Reinitialize(24, 0);
```

Pro přenos naměřených hodnot slouží třída `SensorData`. Povinným argumentem konstruktoru jsou sériové číslo a typ senzoru, ze kterého data pochází. Nepovinným údajem jsou data ve formátu double[]. 

```
SensorData data = new SensorData(sensor.SerialNumber, SensorType.Voltage, new double[1] { 22.05 });
```

Simulace měření:

```csharp
data = sensor.Measure();
// Output measured data
Console.WriteLine("Measured data: {0}", String.Join(","data.Data));
```