﻿using SensorSimulator.Acceleration;
using SensorSimulator.GPS;
using SensorSimulator.Interface;
using SensorSimulator.Temperature;
using SensorSimulator.Tension;
using SensorSimulator.Voltage;
using System;
using System.Diagnostics;

namespace SensorSimulator.API
{
    /// <summary>
    /// API class for sensor measurement simulation
    /// </summary>
    public class Sensor
    {
        /// <summary>
        /// Random number generator
        /// </summary>
        private readonly Random rnd = new Random();

        /// <summary>
        /// Serial number
        /// </summary>
        public string SerialNumber { get { return assignedSensor.SerialNumber; } }

        /// <summary>
        /// Sensor assigned to this instance
        /// </summary>
        private ISensor assignedSensor { get; set; }

        /// <summary>
        /// Sensor type getter
        /// </summary>
        public SensorType Type { get { return (SensorType)assignedSensor.Type; } }

        /// <summary>
        /// Constructor creates sensor with random initial data
        /// </summary>
        /// <param name="Type">Sensor type - supported types can be found in <see cref="API.SensorType"/> </param>
        public Sensor(SensorType Type)
        {
            switch (Type)
            {
                case SensorType.Temperature:
                    // -40 - 100 °C initial value
                    assignedSensor = new TemperatureSensor(rnd.NextDouble() * (100.0 + 40.0) - 40.0);
                    break;
                case SensorType.Voltage:
                    // 22 - 26 V initial value
                    assignedSensor = new VoltageSensor(rnd.NextDouble() * (26.0 - 22.0) + 22.0);
                    break;
                case SensorType.Tension:
                    // 0 - 40 KN  initial value
                    assignedSensor = new TensionSensor(rnd.NextDouble() * (60000.0 - 0.0) + 0.0);
                    break;
                case SensorType.GPS:
                    // Initialize with already set boundaries
                    GpsData iniGPS = new GpsData { Latitude = rnd.NextDouble() * (90.0 + 90.0) - 90.0, Longtitude = rnd.NextDouble() * (180.0 + 180.0) - 180.0 };
                    assignedSensor = new GpsSensor(iniGPS);
                    break;
                case SensorType.Acceleration:
                    // Initialize with already set boundaries
                    AccelerationData iniAcceleration = new AccelerationData { AccelerationX = rnd.NextDouble() * (4.0 + 4.0) - 4.0, AccelerationY = rnd.NextDouble() * (4.0 + 4.0) - 4.0, AccelerationZ = rnd.NextDouble() * (1.0 + 1.0) - 1.0 };
                    assignedSensor = new AccelerationSensor(iniAcceleration);
                    break;
                default:
                    throw new Exception("Invalid sensor type.");
            }
        }

        /// <summary>
        /// Sets measurements range and also reinitialize sensors actual value with middle of set range
        /// </summary>
        /// <param name="Upper">Maximum value of measurements</param>
        /// <param name="Lower">Minimum value of measurements</param>
        /// <returns></returns>
        public bool Reinitialize(double Upper, double Lower)
        {
            try
            {
                if (assignedSensor == null) throw new Exception("Sensor not initialized...");

                switch (assignedSensor.Type)
                {
                    case Types.SensorType.Acceleration:
                    case Types.SensorType.GPS:
                        return Reinitialize();
                    case Types.SensorType.Temperature:
                        assignedSensor = new TemperatureSensor((Upper + Lower) / 2, Upper, Lower);
                        break;
                    case Types.SensorType.Tension:
                        if (Lower < 0) Lower = 0;
                        assignedSensor = new TensionSensor((Upper + Lower) / 2, Upper, Lower);
                        break;
                    case Types.SensorType.Voltage:
                        if (Lower < 0) Lower = 0;
                        assignedSensor = new VoltageSensor((Upper + Lower) / 2, Upper, Lower);
                        break;
                    default:
                        throw new Exception("Unsupported sensor type");
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Reinitialize sensor with new initial value
        /// </summary>
        /// <returns></returns>
        public bool Reinitialize()
        {
            // TODO: Repeating code
            switch (assignedSensor.Type)
            {
                case Types.SensorType.Temperature:
                    // -40 - 100 °C initial value
                    assignedSensor = new TemperatureSensor(rnd.NextDouble() * (100.0 + 40.0) - 40.0);
                    break;
                case Types.SensorType.Voltage:
                    // 22 - 26 V initial value
                    assignedSensor = new VoltageSensor(rnd.NextDouble() * (26.0 - 22.0) + 22.0);
                    break;
                case Types.SensorType.Tension:
                    // 0 - 40 KN  initial value
                    assignedSensor = new TensionSensor(rnd.NextDouble() * (60000.0 - 0.0) + 0.0);
                    break;
                case Types.SensorType.GPS:
                    // Initialize with already set boundaries
                    GpsData iniGPS = new GpsData { Latitude = rnd.NextDouble() * (90.0 + 90.0) - 90.0, Longtitude = rnd.NextDouble() * (180.0 + 180.0) - 180.0 };
                    assignedSensor = new GpsSensor(iniGPS);
                    break;
                case Types.SensorType.Acceleration:
                    // Initialize with already set boundaries
                    AccelerationData iniAcceleration = new AccelerationData { AccelerationX = rnd.NextDouble() * (4.0 + 4.0) - 4.0, AccelerationY = rnd.NextDouble() * (4.0 + 4.0) - 4.0, AccelerationZ = rnd.NextDouble() * (1.0 + 1.0) - 1.0 };
                    assignedSensor = new AccelerationSensor(iniAcceleration);
                    break;
                default:
                    throw new Exception("Invalid sensor type.");
            }
            return true;
        }

        /// <summary>
        /// Try to get measurement from sensor
        /// </summary>
        /// <returns>SensorData structure if successful, else null</returns>
        public SensorData Measure()
        {
            try
            {
                if (assignedSensor == null) throw new Exception("Sensor not initialized...");

                object val = assignedSensor.Measure();

                if (val == null) throw new Exception("Sensor reading failed...");

                double[] data;

                switch (val)
                {
                    case double _:
                        //Debug.WriteLine("#{0}-{1}: {2}", assignedSensor.SerialNumber, ((int)assignedSensor.Type).ToString(), val.ToString());
                        data = new double[1] { (double)val };
                        break;
                    case GpsData g:
                        //Debug.WriteLine("#{0}-{1}: {2}|{3}", assignedSensor.SerialNumber, ((int)assignedSensor.Type).ToString(), g.Latitude.ToString(), g.Longtitude.ToString());
                        data = new double[2] { (double)g.Latitude, (double)g.Longtitude };
                        break;
                    case AccelerationData a:
                        //Debug.WriteLine("#{0}-{1}: {2}|{3}|{4}", assignedSensor.SerialNumber, ((int)assignedSensor.Type).ToString(), a.AccelerationX.ToString(), a.AccelerationY.ToString(), a.AccelerationZ.ToString());
                        data = new double[3] { (double)a.AccelerationX, (double)a.AccelerationY, (double)a.AccelerationZ };
                        break;
                    default:
                        throw new Exception("Invalid data type...");
                }

                if (assignedSensor.SerialNumber == null) throw new Exception("Invalid sensor serial number...");

                SensorData s_data = new SensorData(assignedSensor.SerialNumber, (SensorType)assignedSensor.Type, data);

                return s_data;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
