﻿namespace SensorSimulator.API
{
    /// <summary>
    /// Duplicate of <see cref="Types.SensorType"/> for API users
    /// </summary>
    public enum SensorType
    {
        Temperature,
        Voltage,
        Tension,
        GPS,
        Acceleration
    }
}
