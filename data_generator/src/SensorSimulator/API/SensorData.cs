﻿using System;

namespace SensorSimulator.API
{
    /// <summary>
    /// Sensor measurement storage class
    /// </summary>
    public class SensorData
    {
        /// <summary>
        /// MUST HAVE - Serial number of sensor
        /// </summary>
        public string SensorId { get; }

        /// <summary>
        /// MUST HAVE - Sensor type to determine measured value units etc.
        /// </summary>
        public SensorType DataType { get; }

        /// <summary>
        /// Data itself
        /// </summary>
        public double[] Data { get; }

        /// <summary>
        /// Format measured values as string
        /// </summary>
        /// <returns></returns>
        public string DataToString()
        {
            return "{ " + String.Join(" | ", Data) + " }";
        }

        /// <summary>
        /// Constructor without data assigning that allows adding data later
        /// </summary>
        /// <param name="SerialNumber">Sensor serial number</param>
        /// <param name="DataType">Sensor type</param>
        public SensorData(string SerialNumber, SensorType DataType)
        {
            this.SensorId = SerialNumber;
            this.DataType = DataType;
        }

        /// <summary>
        /// All in one constructor
        /// </summary>
        /// <param name="SensorId">Sensor seral number</param>
        /// <param name="DataType">Sensor type</param>
        /// <param name="Data">Measured data collection</param>
        public SensorData(string SensorId, SensorType DataType, double[] Data)
        {
            this.SensorId = SensorId;
            this.DataType = DataType;
            this.Data = Data;
        }

    }
}
