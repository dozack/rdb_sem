﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace SnumGen
{
    /// <summary>
    /// Random serial number generator that stores generated values to accomplish uniquenes
    /// </summary>
    public static class SerialNumberGenerator
    {
        /// <summary>
        /// Generated serials collection
        /// </summary>
        private static ObservableCollection<string> values = new ObservableCollection<string>();

        /// <summary>
        /// Random number generator
        /// </summary>
        private static readonly Random rnd = new Random();

        /// <summary>
        /// Get unique serial nunber
        /// </summary>
        /// <param name="length">Length of result</param>
        /// <returns>Random serial number</returns>
        public static string GetUnique(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            string tmp = new string(Enumerable.Repeat(chars, length)
              .Select(s => s[rnd.Next(s.Length)]).ToArray());

            while (values.Contains(tmp))
            {
                tmp = GetUnique(length);
            }

            return tmp;
        }

        /// <summary>
        /// Delete serial number from collection
        /// </summary>
        /// <param name="snum">Serial num to be deleted</param>
        public static void Dump(string snum)
        {
            if (values.Contains(snum))
            {
                values.Remove(snum);
            }
        }
    }
}
