﻿using NodeSimulator.Node;

namespace MongoManager.Machine
{
    public class MachineSim
    {
        public string Vin { get; }

        public IotNode AssignedNode { get; }

        public MachineSim(string Vin, IotNode node)
        {
            AssignedNode = node;
            this.Vin = Vin;
        }
    }
}
