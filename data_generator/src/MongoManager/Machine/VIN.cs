﻿using System;
using System.Collections.ObjectModel;
using System.Text;

namespace MongoManager.Machine
{
    public static class VIN
    {
        private static ObservableCollection<string> vinList = new ObservableCollection<string>();

        /// <summary>
        /// Return random VIN code
        /// </summary>
        /// <returns>Generated VIN code</returns>
        private static string GetRandom()
        {
            StringBuilder str = new StringBuilder();
            Random rnd = new Random();

            // Country code number
            str.Append((char)rnd.Next('0', '9'));
            for (int i = 0; i < 16; i++)
            {
                // Year code
                if (i == 9)
                {
                    // Generate random number to decide if number or letter will be put
                    if (Convert.ToBoolean(rnd.Next(0, 2)))
                    {
                        char ch = (char)rnd.Next('A', 'Z');
                        // Dicard IOUZ
                        switch (ch)
                        {
                            case 'I':
                            case 'O':
                            case 'U':
                            case 'Z':
                                ch++;
                                break;
                            default:
                                break;
                        }
                        str.Append(ch);
                    }
                    else
                    {
                        // Discard 0
                        str.Append((char)rnd.Next('1', '9'));
                    }
                }
                // Machine specific device number - only numbers
                else if (i > 10)
                {
                    str.Append((char)rnd.Next('0', '9'));
                }
                else
                {
                    // Generate random number to decide if number or letter will be put
                    if (Convert.ToBoolean(rnd.Next(0, 2)))
                    {
                        str.Append((char)rnd.Next('A', 'Z'));
                    }
                    else
                    {
                        str.Append((char)rnd.Next('0', '9'));
                    }
                }

            }
            // Return completed VIN number
            return str.ToString().ToUpper();
        }

        public static string GetUnique()
        {
            string temp;
            while (vinList.Contains((temp = GetRandom())))
            {

            }
            vinList.Add(temp);

            return temp;
        }
    }
}
