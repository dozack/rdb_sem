﻿using MongoDB.Driver;
using MongoManager.Collections;
using System.Collections.ObjectModel;

namespace MongoManager
{
    public class MongoBroker
    {
        private MongoClient client;

        private string dbName;

        public MongoBroker(string dbName)
        {
            this.dbName = dbName;

            client = new MongoClient();
        }

        public MongoBroker(string connString, string dbName)
        {
            this.dbName = dbName;

            client = new MongoClient(connString);
        }

        public ObservableCollection<string> GetDatabases()
        {
            ObservableCollection<string> res = new ObservableCollection<string>();

            var dbList = client.ListDatabaseNames().ToEnumerable();

            foreach (var name in dbList)
            {
                res.Add(name);
            }

            return res;
        }

        public void DropDatabase()
        {
            client.DropDatabase(dbName);
        }

        public void Push(object Collection)
        {
            switch (Collection)
            {
                case MongoMachine machine:
                    PushMachine(machine);
                    break;
                case MongoNode node:
                    PushNode(node);
                    break;
                case MongoNodeConfig config:
                    PushConfig(config);
                    break;
                case MongoSensor sensor:
                    PushSensor(sensor);
                    break;
                case MongoSensorData data:
                    PushSensorData(data);
                    break;
                default:
                    break;
            }
        }

        private void PushMachine(MongoMachine machine)
        {
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<MongoMachine>("machine");

            collection.InsertOne(machine);
        }

        private void PushNode(MongoNode node)
        {
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<MongoNode>("node");

            collection.InsertOne(node);
        }

        private void PushConfig(MongoNodeConfig config)
        {
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<MongoNodeConfig>("nodeConfig");

            collection.InsertOne(config);
        }

        private void PushSensor(MongoSensor sensor)
        {
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<MongoSensor>("sensor");

            collection.InsertOne(sensor);
        }

        private void PushSensorData(MongoSensorData data)
        {
            var database = client.GetDatabase(dbName);
            var collection = database.GetCollection<MongoSensorData>("sensorData");

            collection.InsertOne(data);
        }
    }
}