﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using SensorSimulator.API;

namespace MongoManager.Collections
{
    public class MongoSensor
    {
        [BsonElement("sensorId")]
        public string SensorId { get { return RefObj.SerialNumber; } }

        [BsonElement("configId")]
        public string ConfigId { get { return DbParent.ConfigId; } }

        [BsonElement("sensorType")]
        public string SensorType { get { return RefObj.Type.ToString(); } }

        public MongoSensor(MongoNodeConfig config, Sensor sensor)
        {
            RefObj = sensor;
            DbParent = config;
        }

        [BsonIgnore]
        public Sensor RefObj { get; }

        [BsonIgnore]
        public MongoNodeConfig DbParent { get; }
    }
}
