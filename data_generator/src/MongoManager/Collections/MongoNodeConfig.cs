﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NodeSimulator.Configuration;
using NodeSimulator.Node;
using System;

namespace MongoManager.Collections
{
    public class MongoNodeConfig
    {
        [BsonElement("configId")]
        public string ConfigId { get { return RefObj.ConfigId; } }

        [BsonElement("nodeId")]
        public string NodeSerial { get; }

        [BsonElement("createDate")]
        public DateTime CreatedAt { get { return RefObj.CreatedAt; } }

        [BsonElement("nodeName")]
        public string NodeName { get { return RefObj.NodeName; } }

        [BsonElement("publishTime")]
        public int PublishTime { get { return RefObj.PublishInterval; } }


        public MongoNodeConfig(IotNode node, NodeConfiguration config)
        {
            RefObj = config;
            NodeSerial = node.SerialNumber;
        }

        public MongoNodeConfig(MongoNode node, NodeConfiguration config)
        {
            DbParent = node;
            RefObj = config;
            NodeSerial = DbParent.NodeId;
        }

        [BsonIgnore]
        public NodeConfiguration RefObj { get; }

        [BsonIgnore]
        public MongoNode DbParent { get; }

    }
}
