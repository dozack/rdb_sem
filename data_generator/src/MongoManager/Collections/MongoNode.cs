﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NodeSimulator.Node;

namespace MongoManager.Collections
{
    public class MongoNode
    {
        [BsonElement("nodeId")]
        public string NodeId { get { return RefObj.SerialNumber; } }

        [BsonElement("machineId")]
        public string MachineVin { get { return DbParent.MachineVIN; } }

        public MongoNode(MongoMachine ParentMachine, IotNode node)
        {
            DbParent = ParentMachine;
            RefObj = node;
        }

        [BsonIgnore]
        public IotNode RefObj { get; }

        [BsonIgnore]
        public MongoMachine DbParent { get; }
    }
}
