﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NodeSimulator.Node;
using SensorSimulator.API;
using SnumGen;
using System;

namespace MongoManager.Collections
{
    public class MongoSensorData
    {
        [BsonElement("dataId")]
        public string DataId { get; }

        [BsonElement("sensorId")]
        public string SensorSerialNumber { get; }

        [BsonElement("nodeId")]
        public string NodeSerialNumber { get; }

        [BsonElement("publishedAt")]
        public DateTime Timestamp { get; } = DateTime.Now;

        [BsonElement("publishedData")]
        public double[] Data { get { return RefObj.Data; } }

        public MongoSensorData(IotNode node, SensorData data)
        {
            RefObj = data;
            SensorSerialNumber = data.SensorId;
            NodeSerialNumber = node.SerialNumber;

            DataId = SerialNumberGenerator.GetUnique(15);
        }

        public MongoSensorData(MongoSensor sensor, SensorData data)
        {
            RefObj = data;
            DbParent = sensor;

            SensorSerialNumber = DbParent.SensorId;
            NodeSerialNumber = DbParent.DbParent.DbParent.NodeId;
        }

        [BsonIgnore]
        public SensorData RefObj { get; }

        [BsonIgnore]
        public MongoSensor DbParent { get; }
    }
}
