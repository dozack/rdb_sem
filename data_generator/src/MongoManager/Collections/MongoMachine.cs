﻿using MongoDB.Bson.Serialization.Attributes;
using MongoManager.Machine;

namespace MongoManager.Collections
{
    public class MongoMachine
    {
        [BsonElement("machineId")]
        public string MachineVIN { get { return RefObj.Vin; } }

        public MongoMachine(MachineSim machine)
        {
            RefObj = machine;
        }

        [BsonIgnore]
        public MachineSim RefObj { get; }
    }
}
