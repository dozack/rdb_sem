﻿using MongoManager.Collections;
using MongoManager.Machine;
using NodeSimulator.Configuration;
using NodeSimulator.NameEngine;
using NodeSimulator.Node;
using SensorSimulator.API;
using System;
using System.Threading;

namespace MongoManager
{
    class MongoTest
    {
        public static MongoBroker broker;

        static void Main(string[] args)
        {
            string[] intro =
            {
                @">>  __  __                         _____ _",
                @">> |  \/  |                       / ____(_)",
                @">> | \  / | ___  _ __   __ _  ___| (___  _ _ __ ___",
                @">> | |\/| |/ _ \| '_ \ / _` |/ _ \\___ \| | '_ ` _ \ ",
                @">> | |  | | (_) | | | | (_| | (_) |___) | | | | | | |",
                @">> |_|  |_|\___/|_| |_|\__, |\___/_____/|_|_| |_| |_|",
                @">>                      __/ |",
                @">>                     |___/"
            };

            foreach (var line in intro)
            {
                Console.WriteLine(line);
            }

            Console.WriteLine(">>");
            Console.WriteLine(">> Author: Zoltan Dolensky");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");

            int nodeCount = 10;

            try
            {
                nodeCount = Convert.ToInt32(args[0]);
            }
            catch
            {
                Console.WriteLine(">> Invalid [node-count] argument, proceeding with default value (10)...");
            }

            Console.ForegroundColor = ConsoleColor.Gray;

            Console.Write(">> Connecting to database on localhost...");

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("OK");
            Console.ForegroundColor = ConsoleColor.Gray;

            broker = new MongoBroker("machineStorage");

            if ((args.Length >= 2) && (args[1] == "-y"))
            {
                Console.Write(">> Dropping old database...");
                broker.DropDatabase();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("OK");
                Console.ForegroundColor = ConsoleColor.Gray;
            }

            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
            Console.WriteLine(">> Simulation initializing...");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");

            #region INIT
            for (int i = 0; i < nodeCount; i++)
            {
                IotNode node = new IotNode(new NodeConfiguration(Name.GetRandomUnique()));

                node.OnConfigUpdate += Node_OnConfigUpdate;
                node.OnStatusChanged += Node_OnStatusChanged;
                node.OnPublish += Node_OnPublish;

                MachineSim machine = new MachineSim(VIN.GetUnique(), node);

                Console.WriteLine(">> Machine VIN: {0}", machine.Vin);
                Console.WriteLine(">> Node name: {0}", node.Configuration.NodeName);

                Console.Write(">> Pushing machine to Mongo...");

                MongoMachine _machine = new MongoMachine(machine);

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("OK");
                Console.ForegroundColor = ConsoleColor.Gray;

                broker.Push(_machine);

                MongoNode _node = new MongoNode(_machine, node);

                Console.Write(">> Pushing node to Mongo...");

                broker.Push(_node);

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("OK");
                Console.ForegroundColor = ConsoleColor.Gray;

                Console.Write(">> Pushing node config to Mongo...");

                MongoNodeConfig _config = new MongoNodeConfig(_node, node.Configuration);

                broker.Push(_config);

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("OK");
                Console.ForegroundColor = ConsoleColor.Gray;


                Console.Write(">> Pushing assigned sensors information to Mongo...");

                foreach (var sensor in _config.RefObj.Sensors)
                {
                    MongoSensor _sensor = new MongoSensor(_config, sensor);

                    broker.Push(_sensor);
                }

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("OK");
                Console.ForegroundColor = ConsoleColor.Gray;

                Console.Write(">> Booting up node...");

                node.Begin();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("OK");
                Console.ForegroundColor = ConsoleColor.Gray;

                Console.WriteLine("-----------------------------------------------------------------------------------------------------");
            }
            #endregion

            Console.WriteLine(">> Simulation initialized successfully...");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");

            while (true)
            {
                Thread.Sleep(1);
            }
        }

        #region EVENTS
        private static void Node_OnPublish(IotNode sender, SensorData dataPublished)
        {
            MongoSensorData _data = new MongoSensorData(sender, dataPublished);

            broker.Push(_data);

            Console.WriteLine(">> {0,-10} | {1,-30} | {2,-30} | {3,-50}", "PUBLISH", sender.Configuration.NodeName, dataPublished.DataType.ToString(), dataPublished.DataToString());
        }


        private static void Node_OnStatusChanged(IotNode sender, NodeStatus statusSet)
        {
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(">> {0,-10} | {1,-30} | {2,-30}", "STATUS", sender.Configuration.NodeName, statusSet.ToString());
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
        }

        private static void Node_OnConfigUpdate(IotNode sender, NodeConfiguration configSet)
        {
            MongoNodeConfig _config = new MongoNodeConfig(sender, configSet);

            broker.Push(_config);

            foreach (var sensor in configSet.Sensors)
            {
                MongoSensor _sensor = new MongoSensor(_config, sensor);

                broker.Push(_sensor);
            }

            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(">> {0,-10} | {1,-30} | {2,-30}", "CONFIG", sender.SerialNumber, sender.Configuration.ConfigId);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
        }
        #endregion
    }
}
