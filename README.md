**Navrhněte a vytvořte databázovou aplikaci, která bude co nejpřehledněji graficky (grafem, ale třeba i na mapě) a tabulkově zobrazovat data ze zemědělských strojů podle zadaných kritérií.**

- stroj je identifikován pomocí VIN
- na každém stroji může být právě jedna IoT jednotka (elektronické zařízení připojené přes GSM k internetu)
- jednotka v pravidelných intervalech (desítky sekund) posílá data
- jednotku lze ze stroje sejmout a použít později na jiném s jinou konfigurací
- jednotky mají své konfigurace a podle jejich nastavení se mohou posílat souřadnice GPS, příznaky, zda je zemědělský stroj například jen v pohybu nebo zda i orá, seje, jaké je napětí na baterii, hodnoty akcelerometrů ve třech směrech, hodnoty tenzometrů na stroji a další údaje
- jednotky mohou být také v takzvaném sleep modu, kdy neposílají nic a stroj stojí.
- jednotka je online, pokud vysílala alespoň ještě pět minut dozadu
- jednotky se rozlišují svým sériovým číslem
- sériové číslo jednotky je jediným povinným posílaným atributem v určitý čas

Připravte si nejprve generátor, který bude simulovat jednotky a umožní DB naplnit daty, kdy každých deset sekund až minutu bude posílat variabilní data z různých jednotek (podle jejich konfigurace). S použitím libovolného programovacího jazyka, prostředí i SŘBD umožněte pohodlné prohlížení dat, filtrování a vyhledávání (podle všech uložených atributů, intervalů časů apod.). Tabulka syrových dat by měla mít řádově stovky tisíc záznamů od různých jednotek v různé časy, přerušené stavem, kdy stroj stojí.